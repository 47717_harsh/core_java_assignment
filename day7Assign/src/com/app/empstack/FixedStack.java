package com.app.empstack;

public class FixedStack implements EmpStack{
	
	private static int top;
	Employee[] employee;
	private int size;
	
	static {
		top = -1;
	}
	public FixedStack()
	{
		this.size = STACK_SIZE;
		employee = new Employee[size];
	}

	@Override
	public void push(Employee emp) {
		
		if(top == size-1)
		{
			System.out.println("Stack Overflow!");
			return;
		}
		else
		{
			top++;
			employee[top] = emp;
			System.out.println("Pushed");
		}
		
	}

	@Override
	public Employee pop() {
		
		Employee emp = null;
		if(top == -1)
		{
			System.out.println("Stack Underflow");
		}
		else
		{
			emp = employee[top];
			top--;
		}
		return emp;
	}
	
	

}
