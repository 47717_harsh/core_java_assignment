package com.app.empstack;

public class GrowableStack implements EmpStack{

	private static int top;
	Employee[] employee;
	private int size;
	private static int growSize;
	
	static {
		top = -1;
		growSize = 5;
	}
	
	public GrowableStack()
	{
		this.size = STACK_SIZE;
		employee = new Employee[size];
	}
	@Override
	public void push(Employee emp) {
		
		if(top == size-1)
		{
			employee = createNew();
			top++;
			employee[top] = emp;
			System.out.println("Pushed");
		}
		else
		{
			top++;
			employee[top] = emp;
			System.out.println("Pushed");
		}
	}

	@Override
	public Employee pop() {
		
		Employee emp = null;
		if(top == -1)
		{
			System.out.println("Stack Underflow");
		}
		else
		{
			emp = employee[top];
			top--;
		}
		return emp;
	}
	
	public Employee[] createNew()
	{
		size += growSize;
		Employee[] emp = new Employee[size];
		for(int i=0;i<employee.length;i++)
		{
			emp[i] = employee[i];
		}
		return emp;
	}
	
	

}
