package com.app.empstack;

public interface EmpStack {
	
	int STACK_SIZE = 5;
	void push(Employee emp);
	Employee pop();
}
