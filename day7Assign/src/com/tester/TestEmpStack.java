package com.tester;

import java.util.Scanner;

import com.app.empstack.EmpStack;
import com.app.empstack.Employee;
import com.app.empstack.FixedStack;
import com.app.empstack.GrowableStack;

public class TestEmpStack {
	
	public static void menu()
	{
		System.out.println("============== MENU ===============");
		System.out.println("1. Fixed Stack");
		System.out.println("2. Growable Stack");
		System.out.println("3. Push Data");
		System.out.println("4. Pop Data");
		System.out.println("5. Exit");
		System.out.println("Select Choice"); 
	}

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		boolean exit = false;
		int choosenStack = 0;
		EmpStack emp = new FixedStack();
		EmpStack emp2 = new GrowableStack();
		
		while(exit != true)
		{
			menu();
			
			switch(sc.nextInt())
			{
			case 1:
				if(choosenStack == 2)
					System.out.println("Stack Already Choosen!");
				else
					choosenStack = 1;
				break;
			case 2:
				if(choosenStack == 1)
					System.out.println("Stack Already Choosen!");
				else
					choosenStack = 2;
				break;
			case 3:
				if(choosenStack == 1)
				{
					System.out.println("Enter Employee Details(id,name,salary) :");
					emp.push(new Employee(sc.nextInt(),sc.next(),sc.nextDouble()));
				}
				else if(choosenStack == 2)
				{
					System.out.println("Enter Employee Details(id,name,salary) :");
					emp2.push(new Employee(sc.nextInt(),sc.next(),sc.nextDouble()));
				}
				else
				{
					System.out.println("No Stack Choosen!!");
				}
				break;
			case 4:
				if(choosenStack == 1)
				{
					System.out.println(emp.pop());
				}
				else if(choosenStack == 2)
				{
					System.out.println(emp2.pop());
				}
				else
				{
					System.out.println("No Stack Choosen!!");
				}
				break;
			case 5:
				System.out.println("Bye!");
				exit = true;
				break;
			default:
			}
		}
		
		sc.close();

	}

}
