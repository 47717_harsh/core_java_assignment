package tester;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Scanner;

import com.app.library.Book;
import com.app.library.BooksCategory;

public class TestBookByHashSet {

	public static void main(String[] args) {
		
		try(Scanner sc = new Scanner(System.in))
		{
			HashSet<Book> library = new HashSet<>();
			for(int i=0;i<5;i++)
			{
				System.out.println("Enter Book Detail(title,category,price,publishDate(yyyy-mm-dd),authorName,quantity) :");
				boolean isAdded = library.add(new Book(sc.next(),BooksCategory.valueOf(sc.next().toUpperCase()),sc.nextDouble(),LocalDate.parse(sc.next()),sc.next(),sc.nextInt()));
				System.out.println("Book Added : "+isAdded);
			}
			for(Book b : library)
				System.out.println(b);
		}

	}

}
