package tester;

import static com.app.library.BooksCategory.valueOf;
import static java.time.LocalDate.parse;
import user_exception.*;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Scanner;

import com.app.library.Book;
import com.app.library.BooksCategory;

public class TestBookByHashMap {

	public static void menu() {
		System.out.println("============= LIBRARY ==============");
		System.out.println("1. Add Book");
		System.out.println("2. Display All");
		System.out.println("3. Issue Single Copy");
		System.out.println("4. Return Book");
		System.out.println("5. Remove Book");
		System.out.println("0. EXIT");
	}

	public static void main(String[] args) {

		try (Scanner sc = new Scanner(System.in)) {
			Book b1 = new Book("title1", valueOf("ACTION"), 1500, parse("2010-11-05"), "hk", 5);
			Book b2 = new Book("title2", valueOf("ADVENTURE"), 1700, parse("2010-11-05"), "hk", 5);
			Book b3 = new Book("title3", valueOf("COMIC"), 1300, parse("2010-11-05"), "hk", 5);
			Book b4 = new Book("title4", valueOf("LITERATURE"), 2500, parse("2010-11-05"), "hk", 5);
			Book b5 = new Book("title5", valueOf("FICTION"), 1577, parse("2010-11-05"), "hk", 5);

			HashMap<String, Book> library = new HashMap<>();

			library.put(b1.getTitle(), b1);
			library.put(b2.getTitle(), b2);
			library.put(b3.getTitle(), b3);
			library.put(b4.getTitle(), b4);
			library.put(b5.getTitle(), b5);
			
			boolean exit = false;
			
			while(!exit)
			{
				menu();
				try {
				switch (sc.nextInt()) {
				case 0:
					System.out.println("Bye");
					exit = true;
					break;
				case 1:
					System.out.println("Enter Book Detail(title,category,price,publishDate(yyyy-mm-dd),authorName,quantity) :");
					String title = sc.next();
					int qty;
					Book newBook = new Book(title,BooksCategory.valueOf(sc.next().toUpperCase()),sc.nextDouble(),LocalDate.parse(sc.next()),sc.next(),qty=sc.nextInt());
					
					Book book = library.get(title);
					if(book!=null)
						book.setQuantity(book.getQuantity()+qty);
					else
						library.put(newBook.getTitle(), newBook);
					System.out.println("Book Added : ");
					break;
				case 2:
					for(Book b : library.values())
						System.out.println(b);
					break;
				case 3:
					System.out.println("Enter Book Title : ");
					book = library.get(sc.next());
					if(book != null)
					{
						if(book.getQuantity() != 0)
							book.setQuantity(book.getQuantity()-1);
						else
							System.out.println("Book Not Available!");
					}
					else
						throw new CustomLibraryException("Invalid Book Title");
					break;
				case 4:
					System.out.println("Enter Book Title : ");
					book = library.get(sc.next());
					if(book != null)
							book.setQuantity(book.getQuantity()+1);
					else
						throw new CustomLibraryException("Invalid Book Title");
					break;
				case 5:
					System.out.println("Enter Book Title : ");
					book = library.remove(sc.next());
					if(book != null)
						System.out.println("Book Removed From Library!");
					else
						throw new CustomLibraryException("Invalid Book Title");
					break;
				default:
					System.out.println("Invalid Choice!!");
					break;
				}
				}catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		}

	}

}
