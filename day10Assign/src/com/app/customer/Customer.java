package com.app.customer;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Customer {

	private String name;
	private String email;
	private String password;
	private double registrationAmount;
	private Date dob;
	private int Id;
	private CustomerType type;
	private AdharAndLocation kyc;//know your customer
	private static int generateCustomerId;//Auto generated customer ID
	public static SimpleDateFormat sdf;
	static {
		generateCustomerId = 100;
		sdf = new SimpleDateFormat("dd/MM/yyyy");
	}

	public Customer(String name, String email, String password, double registrationAmount, Date dob,
			CustomerType type) {
		this.Id = generateCustomerId++;
		this.name = name;
		this.email = email;
		this.password = password;
		this.registrationAmount = registrationAmount;
		this.dob = dob;
		this.type = type;
	}

	public Customer(String email) {
		super();
		this.email = email;
	}
	
	public Customer(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	@Override
	public boolean equals(Object obj) {
		Customer cust = (Customer) obj;
		if (obj instanceof Customer)
			return (email.equals(cust.email));
		return false;
	}
	
	public boolean equalsForLogin(Customer cust)
	{
		if(cust.password != null)
			if(cust instanceof Customer)
				return (email.equals(cust.email) && password.equals(cust.password));
		
		return false;
	}
	@Override
	public String toString() {
		return "Customer [name=" + name + ", email=" + email + ", password=" + password + ", registrationAmount="
				+ registrationAmount + ", dob=" + dob + ", Id=" + Id + ", type=" + type + "kyc :"+kyc+"]";
	}
	
	public void assignAdharAndLocation(String adharNo,String location)
	{
		this.kyc = new AdharAndLocation(adharNo,location);
	}
	
	public AdharAndLocation getKyc() {
		return kyc;
	}


	//Inner Class
	public class AdharAndLocation
	{
		private String adharNo,location;

		public AdharAndLocation(String adharNo, String location) {
			super();
			this.adharNo = adharNo;
			this.location = location;
		}

		@Override
		public String toString() {
			return "AdharAndLocation [adharNo=" + adharNo + ", location=" + location + "]";
		}
		
		
	}
}
