package tester;

import static utils.ValidationRules.ValidateAndCreate;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import com.core.employees.Employee;
import custom_exception.EmployeeServiceException;

public class EmpService {

	static void menu() {
		System.out.println("========== Employee Management ==========");
		System.out.println("1. Add Employee");
		System.out.println("2. View Employee");
		System.out.println("3. View By Joining Date");
		System.out.println("4. Delete Employee");
		System.out.println("5. EXIT");
	}
	
	public static void main(String[] args) {
		
		Map<Integer,Employee> employees = new HashMap<Integer,Employee>();
		boolean exit = false;
		
		try(Scanner sc = new Scanner(System.in))
		{
			while(!exit)
			{
				menu();
				
				try {
					
					switch (sc.nextInt()) {
					case 1:
						System.out.println("Enter Employee Details(id,name,dob,hireDate,salary,department) ");
						Employee e1 = ValidateAndCreate(employees,sc.nextInt(),sc.next(),sc.next(),sc.next(),sc.nextDouble(),sc.next());
						employees.put(e1.getId(),e1);
						break;
					case 2:
						System.out.println("Enter Employee Id :");
						Employee emp = employees.get(sc.nextInt());
						if(emp == null)
							throw new EmployeeServiceException("Employee not found");
						System.out.println(emp);
						break;
					case 3:
						employees.values().stream().
							sorted((o1,o2)->o1.getHireDate().compareTo(o2.getHireDate()))
							.forEach(System.out::println);;
						break;
					case 4:
						System.out.println("Enter Employee Id");
						Employee e = employees.remove(sc.nextInt());
						if(e == null)
							throw new EmployeeServiceException("No Employee with this Id to Delete");
						System.out.println("Employee Deleted Successfully");
						break;
					case 5:
						exit = true;
						break;
					default:
						break;
					}
					
					
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		}
	}

}
