package com.core.employees;

import java.time.LocalDate;

public class Employee {

	private int id;
	private String name;
	private LocalDate dob;
	private LocalDate hireDate;
	private double salary;
	private Departments department;
	
	public LocalDate getHireDate() {
		return hireDate;
	}
	public int getId() {
		return id;
	}
	public Employee(int id, String name, LocalDate dob, LocalDate hireDate, double salary, Departments department) {
		this.id = id;
		this.name = name;
		this.dob = dob;
		this.hireDate = hireDate;
		this.salary = salary;
		this.department = department;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", dob=" + dob + ", hireDate=" + hireDate + ", salary="
				+ salary + ", department=" + department + "]";
	}
	public Departments getDepartment() {
		return department;
	}
	
}
