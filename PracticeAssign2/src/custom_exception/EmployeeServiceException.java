package custom_exception;

@SuppressWarnings("serial")
public class EmployeeServiceException extends Exception {

	public EmployeeServiceException(String msg) {
		super(msg);
	}
}
