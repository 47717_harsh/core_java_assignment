package utils;

import java.time.LocalDate;
import java.time.Period;
import java.util.Map;

import com.core.employees.Departments;
import com.core.employees.Employee;

import custom_exception.EmployeeServiceException;

public class ValidationRules {

	public static void validateId(Map<Integer,Employee> map,int id) throws EmployeeServiceException
	{
		if(map.containsKey(id))
			throw new EmployeeServiceException("Duplicate Id not allowed!");
	}
	public static LocalDate validateDob(Map<Integer,Employee> map,String dob) throws EmployeeServiceException
	{
		LocalDate date = LocalDate.parse(dob);
		Period period = Period.between(date, LocalDate.now());
		if(period.getYears() < 18)
			throw new EmployeeServiceException("Employee age should be more than 18years");
		
		return date;
	}
	
	public static LocalDate validateHireDate(Map<Integer,Employee> map,String hireDate) throws EmployeeServiceException
	{
		LocalDate date = LocalDate.parse(hireDate);
		
		if(date.isAfter(LocalDate.now()))
			throw new EmployeeServiceException("Invalid Hire Date");
		
		return date;
	}
	
	public static Departments validateDepartment(Map<Integer,Employee> map,String dept) throws EmployeeServiceException
	{
		Departments newDept = Departments.valueOf(dept.toUpperCase());
		
		for(Departments d : Departments.values())
			if(d == newDept)
				return newDept;
		
		throw new EmployeeServiceException("Invalid Department");
		
	}
	
	public static Employee ValidateAndCreate(Map<Integer,Employee> map,int id, String name, String dob, String hireDate, double salary, String department) throws EmployeeServiceException
	{
		validateId(map,id);
		LocalDate dateOfBirth = validateDob(map,dob);
		LocalDate hDate = validateHireDate(map,hireDate);
		Departments dept = validateDepartment(map,department);
		return new Employee(id,name,dateOfBirth,hDate,salary,dept);	
	}
}
