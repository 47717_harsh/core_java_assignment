package com.app.library;

import static com.app.library.BooksCategory.valueOf;
import static java.time.LocalDate.parse;

import java.util.HashMap;

public class PopulateLibrary {
	
	public static HashMap<String, Book> populate()
	{
		HashMap<String, Book> library = new HashMap<>();
		
		Book b1 = new Book("dtitle1", valueOf("ACTION"), 1500, parse("2010-11-05"), "harry", 5);
		Book b2 = new Book("atitle2", valueOf("ACTION"), 1700, parse("2010-11-05"), "harsh", 12);
		Book b3 = new Book("ctitle3", valueOf("COMIC"), 1300, parse("2010-11-05"), "vartika", 18);
		Book b4 = new Book("btitle4", valueOf("LITERATURE"), 2500, parse("2010-11-05"), "anshika", 3);
		Book b5 = new Book("etitle5", valueOf("FICTION"), 1577, parse("2010-11-05"), "aakash", 16);

		library.put(b1.getTitle(), b1);
		library.put(b2.getTitle(), b2);
		library.put(b3.getTitle(), b3);
		library.put(b4.getTitle(), b4);
		library.put(b5.getTitle(), b5);
		
		return library;
	}
}
