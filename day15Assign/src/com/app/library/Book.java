package com.app.library;

import java.time.LocalDate;

public class Book {

	private String title;
	private BooksCategory category;
	private double price;
	private LocalDate publishDate;
	private String authorName;
	private int quantity;
	
	public Book(String title, BooksCategory category, double price, LocalDate publishDate, String authorName,
			int quantity) {
		super();
		this.title = title;
		this.category = category;
		this.price = price;
		this.publishDate = publishDate;
		this.authorName = authorName;
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Book [title=" + title + ", category=" + category + ", price=" + price + ", publishDate=" + publishDate
				+ ", authorName=" + authorName + ", quantity=" + quantity + "]";
	}
	
	@Override
	public boolean equals(Object o)
	{
		System.out.println("In Book's equals");
		Book b = (Book)o;
		if(o instanceof Book)
			return title.equals(b.title);
		return false;
	}
	
	public String getTitle() {
		return title;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public BooksCategory getCategory() {
		return category;
	}

	public double getPrice() {
		return price;
	}

	public LocalDate getPublishDate() {
		return publishDate;
	}

	public String getAuthorName() {
		return authorName;
	}

	@Override
	public int hashCode()
	{
		System.out.println("In Books's hashCode");
		return 31*title.hashCode();
	}
	
}
