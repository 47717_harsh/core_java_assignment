package utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.app.library.Book;

public interface ConvertMapToList {
	
	static List<Book> mapToList(Map<String, Book> map)
	{
		List<Book> list = new ArrayList<>(map.values());
		return list;
	}
}
