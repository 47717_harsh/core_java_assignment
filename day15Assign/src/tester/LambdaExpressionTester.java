package tester;

import static com.app.library.PopulateLibrary.populate;
import static utils.ConvertMapToList.*;
import java.time.LocalDate;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import com.app.library.Book;

public class LambdaExpressionTester {

	public static void main(String[] args) {
		
		HashMap<String, Book> library = populate();
		
		System.out.println("Orignal Map : ");
		for(Book b : library.values())
			System.out.println(b);
		
		LocalDate dt = LocalDate.parse("2010-10-01");
		library.values().removeIf(value -> value.getPublishDate().isBefore(dt));
		
		System.out.println("After Removing By Date :");
		library.forEach((t,u)->System.out.println(t+" "+u));
		
		//ArrayList<Book> list = new ArrayList<>(library.values());
		List<Book> list = mapToList(library);
		Collections.sort(list, (v1,v2)->((Double)v1.getPrice()).compareTo(v2.getPrice()));
		
		System.out.println("After Sorting :");
		list.forEach(i->System.out.println(i));

	}

}
