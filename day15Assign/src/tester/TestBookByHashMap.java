package tester;
import static com.app.library.PopulateLibrary.populate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeMap;

import com.app.library.Book;
import com.app.library.BooksCategory;

import user_exception.CustomLibraryException;

public class TestBookByHashMap {

	public static void menu() {
		System.out.println("============= LIBRARY ==============");
		System.out.println("1. Add Book");
		System.out.println("2. Display All");
		System.out.println("3. Issue Single Copy");
		System.out.println("4. Return Book");
		System.out.println("5. Remove Book");
		System.out.println("6. Remove By Author and Category");
		System.out.println("7. Display Book Published Before Specific Date");
		System.out.println("8. Sort By Title(asc)");
		System.out.println("9. Sort By Title(desc)");
		System.out.println("10. Sort By Date and Price");
		System.out.println("0. EXIT");
	}

	public static void main(String[] args) {

		try (Scanner sc = new Scanner(System.in)) {

			HashMap<String, Book> library = populate();
			boolean exit = false;
			
			while(!exit)
			{
				menu();
				try {
				switch (sc.nextInt()) {
				case 0:
					System.out.println("Bye");
					exit = true;
					break;
				case 1:
					System.out.println("Enter Book Detail(title,category,price,publishDate(yyyy-mm-dd),authorName,quantity) :");
					String title = sc.next();
					int qty;
					Book newBook = new Book(title,BooksCategory.valueOf(sc.next().toUpperCase()),sc.nextDouble(),LocalDate.parse(sc.next()),sc.next(),qty=sc.nextInt());
					
					Book book = library.get(title);
					if(book!=null)
						book.setQuantity(book.getQuantity()+qty);
					else
						library.put(newBook.getTitle(), newBook);
					System.out.println("Book Added : ");
					break;
				case 2:
					for(Book b : library.values())
						System.out.println(b);
					break;
				case 3:
					System.out.println("Enter Book Title : ");
					book = library.get(sc.next());
					if(book != null)
					{
						if(book.getQuantity() != 0)
							book.setQuantity(book.getQuantity()-1);
						else
							System.out.println("Book Not Available!");
					}
					else
						throw new CustomLibraryException("Invalid Book Title");
					break;
				case 4:
					System.out.println("Enter Book Title : ");
					book = library.get(sc.next());
					if(book != null)
							book.setQuantity(book.getQuantity()+1);
					else
						throw new CustomLibraryException("Invalid Book Title");
					break;
				case 5:
					System.out.println("Enter Book Title : ");
					book = library.remove(sc.next());
					if(book != null)
						System.out.println("Book Removed From Library!");
					else
						throw new CustomLibraryException("Invalid Book Title");
					break;
				case 6:
					System.out.println("Enter Author name and Book Category :");
					String name = sc.next();
					BooksCategory category = BooksCategory.valueOf(sc.next().toUpperCase());
					Iterator<Book> itr = library.values().iterator();
					while(itr.hasNext())
					{
						if(itr.next().getAuthorName().equals(name))
							if(itr.next().getCategory() == category)
								itr.remove();
					}
					break;
				case 7:
					System.out.println("Enter Date(yyyy-mm-dd) :");
					LocalDate date = LocalDate.parse(sc.next());
					for(Book b : library.values())
						if(b.getPublishDate().isBefore(date))
							System.out.println("Title : "+b.getTitle()+" Author : "+b.getAuthorName()+" Price : "+b.getPrice());
					break;
				case 8:
					TreeMap<String, Book> tm = new TreeMap<>(library);
					for(Book b : tm.values())
						System.out.println(b);
					break;
				case 9:
					TreeMap<String, Book> tm1 = new TreeMap<>(new Comparator<String>() {

						@Override
						public int compare(String o1, String o2) {
							return o2.compareTo(o1);
						}
					});
					tm1.putAll(library);
					for(Book b : tm1.values())
						System.out.println(b);
						
					break;
				case 10:
					ArrayList<Book> al = new ArrayList<>(library.values());
					//using anonymous inner class
//					Collections.sort(al,new Comparator<Book>() {
//
//						@Override
//						public int compare(Book o1, Book o2) {
//							
//							int retVal = o1.getPublishDate().compareTo(o2.getPublishDate());
//							 if(retVal == 0)
//								 return ((Double)o1.getPrice()).compareTo(o2.getPrice());
//							 return retVal;
//							
//						}
//					});
					//using lambda expression
					Collections.sort(al,(o1,o2)->{
						int retVal = o1.getPublishDate().compareTo(o2.getPublishDate());
						 if(retVal == 0)
							 return ((Double)o1.getPrice()).compareTo(o2.getPrice());
						 return retVal;
					});
					
					for(Book b : al)
						System.out.println(b);
					break;
				default:
					System.out.println("Invalid Choice!!");
					break;
				}
				}catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		}

	}

}
