package user_exception;

@SuppressWarnings("serial")
public class CustomLibraryException extends Exception{

	public CustomLibraryException(String message) {
		super(message);
	}
	
}
