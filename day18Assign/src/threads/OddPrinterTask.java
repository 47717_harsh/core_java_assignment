package threads;

public class OddPrinterTask implements Runnable{
	
	private int start;
	private int end;
	
	
	public OddPrinterTask(int start, int end) {
		super();
		this.start = start;
		this.end = end;
	}

	@Override
	public void run() {
		
		try {
			for(int i = start;i<=end;i++)
			{
				if(i%2 != 0)
				{
					System.out.println("Odd : "+i);
					Thread.sleep(250);
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

}
