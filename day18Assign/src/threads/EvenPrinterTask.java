package threads;

public class EvenPrinterTask implements Runnable{
	
	private int start;
	private int end;
	
	public EvenPrinterTask(int start,int end) {
		this.start = start;
		this.end = end;
	}
	
	@Override
	public void run() {
		  
		try {
			for(int i = start;i<=end;i++)
			{
				if(i%2 == 0)
				{
					System.out.println("Even : "+i);
					Thread.sleep(200);
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
