package threads;

public class PrimePrinterTask implements Runnable {

	private int start;
	private int end;

	public PrimePrinterTask(int start, int end) {
		super();
		this.start = start;
		this.end = end;
	}

	@Override
	public void run() {

		try {

			for (int i = start; i <= end; i++) {
				if (isPrime(i)) {
					System.out.println("Prime : " + i);
					Thread.sleep(300);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	static boolean isPrime(int n) {
		// Corner case
		if (n <= 1)
			return false;

		// Check from 2 to n-1
		for (int i = 2; i < n; i++)
			if (n % i == 0)
				return false;

		return true;
	}

}
