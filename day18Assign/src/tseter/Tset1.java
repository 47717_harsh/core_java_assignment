package tseter;

import java.util.Scanner;

import threads.EvenPrinterTask;
import threads.OddPrinterTask;
import threads.PrimePrinterTask;

public class Tset1 {
	
	
	public static void main(String[] args) {
		
		System.out.println("Enter Start and End Point :");
		try(Scanner sc = new Scanner(System.in))
		{
			int start = sc.nextInt();
			int end = sc.nextInt();
			
			Thread t1 = new Thread(new EvenPrinterTask(start,end),"Even");
			Thread t2 = new Thread(new OddPrinterTask(start,end),"Odd");
			Thread t3 = new Thread(new PrimePrinterTask(start,end),"Prime");
			t1.start();
			t2.start();
			t3.start();
			
//			Thread t4 = new Thread(()->{
//				try {
//					for(int i=start;i<=end;i++)
//					{
//						System.out.println("MyThread : "+i*2);
//						Thread.sleep(100);
//					}
//				} catch (InterruptedException e) {
//					e.printStackTrace();
//				}
//			},"MyThread");
			
			
			t1.join();
			t2.join();
			t3.join();
			
			System.out.println("Main Over....");
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	

}
