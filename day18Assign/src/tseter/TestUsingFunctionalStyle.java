package tseter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class TestUsingFunctionalStyle {
	
	 static boolean isPrime(int n)
	    {
	        if (n <= 1)
	            return false;
	  
	        for (int i = 2; i < n; i++)
	            if (n % i == 0)
	                return false;
	  
	        return true;
	    }

	public static void main(String[] args) {
		
		System.out.println("Enter Start and End Points :");
		try(Scanner sc = new Scanner(System.in))
		{
			int start = sc.nextInt();
			int end = sc.nextInt();
			
			Thread t1 = new Thread(()->{
				try {
					for(int i = start;i<=end;i++)
					{
						if(i%2 == 0)
						{
							System.out.println("Even : "+i);
							Thread.sleep(200);
						}
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}, "Even");
			
			Thread t2 = new Thread(()->{
				try {
					for(int i = start;i<=end;i++)
					{
						if(i%2 != 0)
						{
							System.out.println("Odd : "+i);
							Thread.sleep(250);
						}
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}, "Odd");
			
			Thread t3 = new Thread(()->{
				try {
					
					for(int i=start;i<=end;i++) {
						if(isPrime(i))
						{
							System.out.println("Prime : "+i);
							Thread.sleep(300);
						}
					}
				}catch (Exception e) {
					e.printStackTrace();
				}
			}, "Prime");
			
			ArrayList<Thread> list = new ArrayList<>(Arrays.asList(t1,t2,t3));
			list.forEach(i->i.start());
			
			list.forEach(i->{
				try {
					i.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			});
			System.out.println("Main Over....");
		}
	}

}
