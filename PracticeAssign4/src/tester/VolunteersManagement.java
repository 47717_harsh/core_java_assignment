package tester;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import static utils.ValidationRules.*;
import static utils.SerDeSerUtils.*;
import com.app.volunteer.Volunteer;

public class VolunteersManagement {

	static void menu() {
		System.out.println("========== Volunteer Management ==========");
		System.out.println("1. Add volunteer");
		System.out.println("2. Update Information");
		System.out.println("3. Store in File");
		System.out.println("4. Restore from File");
		System.out.println("5. View By Hobbies");
		System.out.println("6. EXIT");
	}
	
	public static void main(String[] args) {
		
		List<Volunteer> vList = new ArrayList<>();
		boolean exit = false;
		
		try(Scanner sc = new Scanner(System.in))
		{
			while(!exit)
			{
				menu();
				try {
					
					switch (sc.nextInt()) {
					
					case 1:
						System.out.println("Enter volunteers detail (id,vName,dob(yyyy-mm-dd),isAvailable)");
						int id = sc.nextInt();
						String name = sc.next();
						String dob = sc.next();
						boolean isAvailable = sc.nextBoolean();
						System.out.println("Enter Hobbies :");
						List<String> hobbie = new ArrayList<>();
						boolean done = false;
						while(!done)
						{
							hobbie.add(sc.next());
							System.out.println("Want to add more y/n :");
							String choice = sc.next();
							if( !choice.equals("y"))
								done = true;
						}
						vList.add(validateAndCreate(vList,id,name,hobbie,dob,isAvailable));		
						break;
					case 2:
						System.out.println("Enter File Name :");
						String fileName = sc.next();
						List<Volunteer> list = restoreVolunteerDetails(fileName);
						System.out.println("Enter Volunteer name and dob for Updation");
						Volunteer v = findByNameAndDob(list,sc.next(),sc.next());
						System.out.println("Enter updated Hobbies :");
						hobbie = new ArrayList<>();
						done = false;
						while(!done)
						{
							hobbie.add(sc.next());
							System.out.println("Want to add more y/n :");
							String choice = sc.next();
							if( !choice.equals("y"))
								done = true;
						}
						v.setHobbies(hobbie);
						storeVolunteerDetails(list,fileName);
						break;
					case 3:
						System.out.println("Enter File Name :");
						storeVolunteerDetails(vList,sc.next());
						break;
					case 4:
						System.out.println("Enter File Name :");
						restoreVolunteerDetails(sc.next()).stream()
								.forEach(System.out::println);
						break;
					case 5:
						System.out.println("Enter Hobbie to search");
						String hobb = sc.next();
						vList.stream()
							.filter(i -> i.getHobbies().contains(hobb))
							.forEach(System.out::println);
						break;
					case 6:
						exit = true;
						break;
					default :
						break;
					
					}
				}catch(Exception e) {
					System.out.println(e);
				}
			}
		}
	}

}
