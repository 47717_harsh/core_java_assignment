package com.app.volunteer;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public class Volunteer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9191518066841624776L;
	private int id;
	private String vName;
	private List<String> hobbies;
	private LocalDate dob;
	private boolean isAvailable;
	
	public Volunteer(int id, String vName, List<String> hobbies, LocalDate dob, boolean isAvailable) {
		super();
		this.id = id;
		this.vName = vName;
		this.hobbies = hobbies;
		this.dob = dob;
		this.isAvailable = isAvailable;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setvName(String vName) {
		this.vName = vName;
	}
	public void setHobbies(List<String> hobbies) {
		this.hobbies = hobbies;
	}
	public void setDob(LocalDate dob) {
		this.dob = dob;
	}
	public void setAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	public Volunteer(String name, LocalDate dateOfBirt) {
		this.vName = name;
		this.dob = dateOfBirt;
	}
	@Override
	public String toString() {
		return "Volunteer [id=" + id + ", vName=" + vName + ", hobbies=" + hobbies + ", dob=" + dob + ", isAvailable="
				+ isAvailable + "]";
	}
	public int getId() {
		return id;
	}
	public String getvName() {
		return vName;
	}
	public List<String> getHobbies() {
		return hobbies;
	}
	public LocalDate getDob() {
		return dob;
	}
	public boolean isAvailable() {
		return isAvailable;
	}

	
	@Override
	public boolean equals(Object obj) {
		Volunteer v = (Volunteer)obj;
		if(obj instanceof Volunteer)
		{
			return (vName.equals(v.vName) && dob.equals(v.dob));
		}
		return false;
	}
	
	
	
}
