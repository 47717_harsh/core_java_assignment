package custom_exception;

public class VolunteerManagementException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public VolunteerManagementException(String message) {
		super(message);
	}
	
}
