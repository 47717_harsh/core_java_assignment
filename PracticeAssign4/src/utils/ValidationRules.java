package utils;

import java.time.LocalDate;
import java.util.List;

import com.app.volunteer.Volunteer;

import custom_exception.VolunteerManagementException;

public class ValidationRules {
	
	public static LocalDate validateDob(String dob) throws VolunteerManagementException
	{
		LocalDate dateOfBirt = LocalDate.parse(dob);
		if(dateOfBirt.isBefore(LocalDate.parse("1990-01-01")))
			throw new VolunteerManagementException("DOB must be greater than 1-1-1990");
		return dateOfBirt;
	}
	
	public static void validateDuplicate(List<Volunteer> list,String name,String dob) throws VolunteerManagementException
	{
		LocalDate dateOfBirt = LocalDate.parse(dob);
		Volunteer vol = new Volunteer(name,dateOfBirt);
		for(Volunteer v : list)
		{
			if(v.equals(vol))
				throw new VolunteerManagementException("Duplicate Entry Not Allowed");
		}
	}
	
	public static Volunteer validateAndCreate(List<Volunteer> list,int id, String vName, List<String> hobbies, String dob, boolean isAvailable) throws VolunteerManagementException
	{
		LocalDate dateOfBirth = validateDob(dob);
		validateDuplicate(list,vName,dob);
		return new Volunteer(id,vName,hobbies,dateOfBirth,isAvailable);
	}
	
	public static Volunteer findByNameAndDob(List<Volunteer> list,String name,String dob) throws VolunteerManagementException
	{
		LocalDate dateOfBirt = LocalDate.parse(dob);
		Volunteer vol = new Volunteer(name,dateOfBirt);
		for(Volunteer v : list)
			if(v.equals(vol))
				return v;
		throw new VolunteerManagementException("Volunteer Not Found");
		
	}
}
