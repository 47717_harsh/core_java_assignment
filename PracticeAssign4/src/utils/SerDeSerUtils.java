package utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;
import com.app.volunteer.Volunteer;

public interface SerDeSerUtils {
	
	static void storeVolunteerDetails(List<Volunteer> sList,String fileName) throws FileNotFoundException, IOException
	{
		try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName)))
		{
			out.writeObject(sList);
		}
	}
	
	@SuppressWarnings("unchecked")
	static List<Volunteer> restoreVolunteerDetails(String fileName) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		try(ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName)))
		{
			return (List<Volunteer>) in.readObject();
		}
	}
}
