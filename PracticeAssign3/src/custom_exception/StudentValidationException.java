package custom_exception;

@SuppressWarnings("serial")
public class StudentValidationException extends Exception {

	public StudentValidationException(String message) {
		super(message);
	}
	
}
