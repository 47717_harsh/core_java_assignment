package com.app.student;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

public class Student implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3194932376061455308L;
	private  int id;
	private String studentName;
	private int age;
	private LocalDate registrationDate;
	private List<String> courseList;
	static int generateId;
	static {
		generateId = 100;
	}
	
	public Student(String studentName, int age, List<String> courseList,LocalDate registrationDate) {
		this.studentName = studentName;
		this.age = age;
		this.courseList = courseList;
		this.id = ++generateId;
		this.registrationDate = registrationDate;
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", studentName=" + studentName + ", age=" + age + ", registrationDate="
				+ registrationDate + ", courseList=" + courseList + "]";
	}

	public List<String> getCourseList() {
		return courseList;
	}
}
