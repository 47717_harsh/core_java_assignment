package tester;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import static utils.StudentValidationRule.*;
import com.app.student.Student;
import static utils.SerDeSerUtils.*;

public class StudentEnrollApp {

	static void menu() {
		System.out.println("========== Student Management ==========");
		System.out.println("1. Enroll Student");
		System.out.println("2. Course Wise StudentList");
		System.out.println("3. Store in File");
		System.out.println("4. Restore from File");
		System.out.println("5. EXIT");
	}
	
	public static void main(String[] args) {
		
		boolean exit = false;
		List<Student> sList = new ArrayList<>();
		try(Scanner sc = new Scanner(System.in))
		{
			while(!exit)
			{
				menu();
				try {
					switch (sc.nextInt()) {
					case 1:
						System.out.println("Enter Student Detalis(studentName,age,registrationDate(yyyy-mm-dd)) ");
						String name = sc.next();
						int age = sc.nextInt();
						String date = sc.next();
						System.out.println("Enter courseList");
						List<String> l= new ArrayList<>();
						boolean done = false;
						while(!done)
						{
							l.add(sc.next());
							System.out.println("Want to add more Y/N :");
							String choice = sc.next();
							if( !choice.equals("Y"))
								done = true;
						}
						sList.add(validateAndCreate(name,age,l,date));
						break;
					case 2:
						System.out.println("Enter Course Name :");
						String course = sc.next();
						sList.stream()
							.filter(i -> i.getCourseList().contains(course))
							.forEach(System.out::println);
						break;
					case 3:
						System.out.println("Enter File Name");
						storeStudentDetails(sList,sc.next());
						break;
					case 4:
						System.out.println("Enter File Name");
						restoreStudentDetails(sc.next()).stream()
							.forEach(System.out::println);
						break;
					case 5:
						exit = true;
						break;
					default:
						exit = true;
						break;
					}
					
				} catch (Exception e) {
					System.out.println(e);
				}
				sc.nextLine();
			}
		}

	}

}
