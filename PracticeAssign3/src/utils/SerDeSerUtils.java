package utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.List;

import com.app.student.Student;

public interface SerDeSerUtils {
	
	static void storeStudentDetails(List<Student> sList,String fileName) throws FileNotFoundException, IOException
	{
		try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName)))
		{
			out.writeObject(sList);
		}
	}
	
	@SuppressWarnings("unchecked")
	static List<Student> restoreStudentDetails(String fileName) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		try(ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName)))
		{
			return (List<Student>) in.readObject();
		}
	}
}
