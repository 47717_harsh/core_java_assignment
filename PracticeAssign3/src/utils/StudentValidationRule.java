package utils;

import java.time.LocalDate;
import java.util.List;

import com.app.student.Student;

import custom_exception.StudentValidationException;

public class StudentValidationRule {

	public static LocalDate validateRegistrationDate(String date) throws StudentValidationException
	{
		LocalDate regDate = LocalDate.parse(date);
		if(regDate.isAfter(LocalDate.now()))
			throw new StudentValidationException("Invalid Registration Date");
		return regDate;
	}
	
	public static void checkForDuplicateEntry(List<String> list) throws StudentValidationException
	{
		for(String s : list)
		{
			if(list.indexOf(s) != list.lastIndexOf(s))
				throw new StudentValidationException("Only Once per Student per course is allowed!!");
		}
	}
	
	public static Student validateAndCreate(String studentName, int age, List<String> courseList,String registrationDate) throws StudentValidationException
	{
		LocalDate regDate = validateRegistrationDate(registrationDate);
		checkForDuplicateEntry(courseList);
		return new Student(studentName,age,courseList,regDate);
	}
}
