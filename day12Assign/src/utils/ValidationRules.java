package utils;

import static com.app.customer.Customer.sdf;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import com.app.customer.Customer;
import com.app.customer.CustomerType;

import custom_exception.CustomerHandlingException;

public class ValidationRules {

	public static void validateEmail(String email) throws CustomerHandlingException {
		String regex = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.+-]+\\.com$";
		if (!email.matches(regex)) {
			throw new CustomerHandlingException("Invalid email!!");
		}
	}

	public static void validatePassword(String password) throws CustomerHandlingException {
		if (password.length() < 4 || password.length() > 10) {
			throw new CustomerHandlingException("Invalid Password!!");
		}
	}

	public static Date validateDob(String dob) throws ParseException, CustomerHandlingException {
		Date d1 = sdf.parse(dob);
		Date begin = sdf.parse("01/01/1995");
		if (!d1.before(begin)) {
			throw new CustomerHandlingException("Invalid Date!!");
		}
		return d1;
	}

	public static void validateDuplicateCustomer(ArrayList<Customer> customerList, String email)
			throws CustomerHandlingException {
		Customer newCustomer = new Customer(email);

		if(customerList.contains(newCustomer))
			throw new CustomerHandlingException("Email Id already exists");
	}

	public static void validateCustomerPlan(String customerType, double registrationAmount)
			throws CustomerHandlingException {
		CustomerType newCustomer = CustomerType.valueOf(customerType.toUpperCase());
		for (CustomerType c : CustomerType.values()) {
			if (c.equals(newCustomer) && c.getRegistrationAmount() != registrationAmount) {
				throw new CustomerHandlingException("Invalid Plan Selection!!");
			}
		}
	}

	public static Customer ValidateAndCreateNew(String name, String email, String password, double registrationAmount,
			String dob, String type, ArrayList<Customer> customerList) throws CustomerHandlingException, ParseException {
		validateDuplicateCustomer(customerList, email);
		validateEmail(email);
		Date newDob = validateDob(dob);
		validatePassword(password);
		validateCustomerPlan(type, registrationAmount);
		return new Customer(name, email, password, registrationAmount, newDob,
				CustomerType.valueOf(type.toUpperCase()));
	}

	public static Customer checkLoginCredential(ArrayList<Customer> customerList, String email, String password)
			throws CustomerHandlingException {
		Customer newCustomer = new Customer(email, password);
		for (Customer c : customerList) 
				if (c.equalsForLogin(newCustomer)) 
					return c;
				
			throw new CustomerHandlingException("Invalid email or password!!");
	}

	public static Customer checkEmailForAdhar(String email, ArrayList<Customer> customerList) throws CustomerHandlingException {
		Customer newCust = new Customer(email);

		int index = customerList.indexOf(newCust);
		if(index == -1)
			throw new CustomerHandlingException("Email not exists Please enter valid email!");
		return customerList.get(index);
	}

}
