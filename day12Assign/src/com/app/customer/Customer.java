package com.app.customer;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class Customer implements Comparable<Customer>{

	private String name;
	private String email;
	private String password;
	private double registrationAmount;
	private Date dob;
	private int Id;
	private CustomerType type;
	private AdharAndLocation kyc;//know your customer
	private static int generateCustomerId;//Auto generated customer ID
	public static SimpleDateFormat sdf;
	static {
		generateCustomerId = 100;
		sdf = new SimpleDateFormat("dd/MM/yyyy");
	}

	public Customer(String name, String email, String password, double registrationAmount, Date dob,
			CustomerType type) {
		this.Id = generateCustomerId++;
		this.name = name;
		this.email = email;
		this.password = password;
		this.registrationAmount = registrationAmount;
		this.dob = dob;
		this.type = type;
	}
	
	public Customer(String email) {
		super();
		this.email = email;
	}
	
	public Customer(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public CustomerType getType() {
		return type;
	}

	public void setType(CustomerType type) {
		this.type = type;
	}

	public Date getDob() {
		return dob;
	}

	@Override
	public boolean equals(Object obj) {
		Customer cust = (Customer) obj;
		if (obj instanceof Customer)
			return (email.equals(cust.email));
		return false;
	}
	
	public boolean equalsForLogin(Customer cust)
	{
		if(cust.password != null)
			if(cust instanceof Customer)
				return (email.equals(cust.email) && password.equals(cust.password));
		
		return false;
	}
	@Override
	public String toString() {
		return "Customer [name=" + name + ", email=" + email + ", password=" + password + ", registrationAmount="
				+ registrationAmount + ", dob=" + dob + ", Id=" + Id + ", type=" + type + "kyc :"+kyc+"]";
	}
	
	public void assignAdharAndLocation(String adharNo,String location)
	{
		this.kyc = new AdharAndLocation(adharNo,location);
	}
	
	public AdharAndLocation getKyc() {
		return kyc;
	}


	//Inner Class
	public class AdharAndLocation
	{
		private String adharNo,location;

		public AdharAndLocation(String adharNo, String location) {
			super();
			this.adharNo = adharNo;
			this.location = location;
		}

		@Override
		public String toString() {
			return "AdharAndLocation [adharNo=" + adharNo + ", location=" + location + "]";
		}
		
		
	}

	@Override
	public int compareTo(Customer c) {
		return this.email.compareTo(c.email);
	}
	
	public static final Comparator<Customer> cst = new Comparator<Customer>() {

		@Override
		public int compare(Customer o1, Customer o2) {
			
			int retVal = o1.getDob().compareTo(o2.getDob());
			if(retVal == 0)
				return o1.getType().compareTo(o2.getType());
			return -retVal;
		}
		
	};
}
