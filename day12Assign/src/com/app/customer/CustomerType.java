package com.app.customer;

public enum CustomerType {
	SILVER(500),GOLD(1000),DIAMOND(1500),PLATINUM(2000);
	
	private double registrationAmount;

	private CustomerType(int registrationAmount) {
		this.registrationAmount = registrationAmount;
	}

	@Override
	public String toString() {
		return name()+" : @"+registrationAmount;
	}

	public double getRegistrationAmount() {
		return registrationAmount;
	}

	public void setRegistrationAmount(int registrationAmount) {
		this.registrationAmount = registrationAmount;
	}
	
}
