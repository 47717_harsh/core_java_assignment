package tester;

import static utils.ValidationRules.ValidateAndCreateNew;
import static utils.ValidationRules.checkEmailForAdhar;
import static utils.ValidationRules.checkLoginCredential;
import static utils.ValidationRules.validatePassword;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import com.app.customer.Customer;
import com.app.customer.CustomerType;

import custom_exception.CustomerHandlingException;
import custom_ordering.CustomerDobComparator;

public class CustomerManagementSystem {
	
	public static void menu() {
		System.out.println("============== CUSTOMER =============");
		System.out.println("1. Add Customer");
		System.out.println("2. Display All Customer Details");
		System.out.println("3. Customer Login");
		System.out.println("4. Link Adhar Card");
		System.out.println("5. Change password");
		System.out.println("6. Upgrad Plans For Senior Citizen");
		System.out.println("7. Remov Silver Plan");
		System.out.println("8. Sort as per email(asc)");
		System.out.println("9. Sort as per DOB(desc)");
		System.out.println("10. Sort as per DOB and Customer Plan");
		System.out.println("11. Exit");
		System.out.println("Select Choice");
	}

	public static void main(String[] args) {
		
		try(Scanner sc = new Scanner(System.in)){
			
			ArrayList<Customer> customerList = new ArrayList<>();
			boolean exit = false;
			
			while(!exit)
			{
				menu();
				try {
					switch (sc.nextInt()) {
					
					case 1:
						System.out.println("Enter Customer Details(name,email,password,registrationAmount,dob,Registration Type) :");
						System.out.println("Registration Types Options:");
						for(CustomerType c : CustomerType.values())
						{
							System.out.println(c);
						}
						customerList.add(ValidateAndCreateNew(sc.next(),sc.next(),sc.next(),sc.nextDouble(),sc.next(),sc.next(),customerList));
						break;
					case 2:
						for(Customer c : customerList)
								System.out.println(c);
						break;
					case 3:
						System.out.println("Enter Email and Password :");
						checkLoginCredential(customerList,sc.next(),sc.next());
						System.out.println("Login Successfull!!");
						break;
					case 4:
						System.out.println("Enter email address: ");
						Customer c = checkEmailForAdhar(sc.next(),customerList);
						if(c.getKyc() == null)
						{
							System.out.println("Enter Adhar Number and location: ");
							c.assignAdharAndLocation(sc.next(), sc.next());
							System.out.println("Adhar and Location Linked Successfully!");
						}
						else
							throw new CustomerHandlingException("Adhar and Location Already Linked");
						break;
					case 5:
						System.out.println("Enter email and old password :");
						c = checkLoginCredential(customerList,sc.next(),sc.next());
						if(c.getKyc() != null)
						{
							System.out.println("Enter new password : ");
							String pass = sc.next();
							validatePassword(pass);
							c.setPassword(pass);
						}
						else
							throw new CustomerHandlingException("Adhar Not Linked");
						break;
					case 6:
						Date d1 = new Date();
						CustomerType type = CustomerType.valueOf("DIAMOND");
						CustomerType newType = CustomerType.valueOf("PLATINUM");
						for(Customer c1 : customerList)
						{
							long dateDiff = d1.getTime() - c1.getDob().getTime();
							long days = TimeUnit.MILLISECONDS.toDays(dateDiff);
							if(days/365 > 60)
								if(c1.getType().equals(type))
									c1.setType(newType);
						}
						break;
					case 7:
						Iterator<Customer> custItr = customerList.iterator();
						while(custItr.hasNext())
						{
							if(custItr.next().getType() == CustomerType.SILVER)
								custItr.remove();
						}
						break;
					case 8:
						Collections.sort(customerList);
						break;
					case 9:
						Collections.sort(customerList,new CustomerDobComparator());
						break;
					case 10:
						Collections.sort(customerList,Customer.cst);
						break;
					case 11:
						System.out.println("Bye");
						exit = true;
						break;
					default:
						break;
					}
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

}
