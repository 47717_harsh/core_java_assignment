package custom_ordering;

import java.util.Comparator;

import com.app.customer.Customer;

public class CustomerDobComparator implements Comparator<Customer>{
	
	@Override
	public int compare(Customer o1, Customer o2) {
		if(o1.getDob().compareTo(o2.getDob()) > 0)
			return -1;
		else if(o1.getDob().compareTo(o2.getDob()) < 0)
			return 1;
		return 0;
	}
}
