package com.app.student_info;

import java.time.LocalDate;

public class Student {
	
	private String rollNo;
	private String name;
	private LocalDate dob;
	private Subjects subject;
	private double gpa;
	
	public Student(String rollNo, String name, LocalDate dob, Subjects subject, double gpa) {
		super();
		this.rollNo = rollNo;
		this.name = name;
		this.dob = dob;
		this.subject = subject;
		this.gpa = gpa;
	}

	@Override
	public String toString() {
		return "Student [rollNo=" + rollNo + ", name=" + name + ", dob=" + dob + ", subject=" + subject + ", gpa=" + gpa
				+ "]";
	}

	public String getRollNo() {
		return rollNo;
	}

	public String getName() {
		return name;
	}

	public LocalDate getDob() {
		return dob;
	}

	public Subjects getSubject() {
		return subject;
	}

	public double getGpa() {
		return gpa;
	}
	
}
