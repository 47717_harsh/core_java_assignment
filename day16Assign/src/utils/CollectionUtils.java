package utils;

import static com.app.student_info.Subjects.valueOf;
import static java.time.LocalDate.parse;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.app.student_info.Student;

public interface CollectionUtils {
	/*
	 * Add a static method to return populated list of students (at least 5 records)
	 * Add another static method to populate a HashMap from the list
	 */
	static List<Student> populateStudentList()
	{
		List<Student> list = new ArrayList<>();
		list.add(new Student("101", "Harsh", parse("1997-11-19"), valueOf("JAVA"), 9));
		list.add(new Student("97", "viplav", parse("1997-05-10"), valueOf("REACT"), 8.4));
		list.add(new Student("105", "sourabh", parse("1998-10-15"), valueOf("JAVA"), 3.7));
		list.add(new Student("74", "chinu", parse("1997-11-17"), valueOf("REACT"), 7.8));
		list.add(new Student("133", "nikhil", parse("1996-03-22"), valueOf("JAVA"), 4.7));
		return list;
	}
	
	static Map<String, Student> mapFromList(List<Student> list)
	{
		Map<String, Student> map = list.stream()
				.collect(Collectors.toMap(Student::getRollNo, val->val));
		return map;
	}
}
