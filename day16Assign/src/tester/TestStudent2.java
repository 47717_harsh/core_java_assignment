package tester;

import static utils.CollectionUtils.populateStudentList;

import java.util.List;
import java.util.Scanner;

import com.app.student_info.Student;
import com.app.student_info.Subjects;

public class TestStudent2 {

	public static void main(String[] args) {
		
		try(Scanner sc = new Scanner(System.in))
		{
			List<Student> list = populateStudentList();
			System.out.println("Enter Subject :");
			
			Subjects subject = Subjects.valueOf(sc.next().toUpperCase());
			list.stream()
				.filter(i->i.getSubject() == subject)
					.map(Student::getGpa)
					.reduce(Double::sum)
					.ifPresent(System.out::println);
				
			list.stream()
				.filter(i->i.getSubject() == subject)
				.map(s->s.getGpa())
				.reduce((a,b)->a*b)
				.ifPresent(i->System.out.println("Sum : "+i));
			
		}
		

	}

}
