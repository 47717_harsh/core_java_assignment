package tester;

import static utils.CollectionUtils.populateStudentList;

import java.util.List;
import java.util.Scanner;

import com.app.student_info.Student;
import com.app.student_info.Subjects;

public class TestStudent3 {

	public static void main(String[] args) {
		
		try(Scanner sc = new Scanner(System.in))
		{
			List<Student> list = populateStudentList();
			System.out.println("Enter Subject :");
			
			Subjects subject = Subjects.valueOf(sc.next().toUpperCase());
			
			list.stream()
				.filter(i->i.getSubject() == subject)
				.mapToDouble(Student::getGpa)
				.average().ifPresent(System.out::println);	
			
			Subjects subject1 = Subjects.valueOf(sc.next().toUpperCase());
			System.out.println("Enter Subject :");
			list.stream()
				.filter(i->i.getSubject() == subject1)
				.max((o1,o2)->((Double)o1.getGpa()).compareTo(o2.getGpa()))
				.ifPresent(System.out::println);
		}
		

	}

}
