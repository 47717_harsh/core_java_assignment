package tester;

import java.util.List;

import com.app.student_info.Student;
import static utils.CollectionUtils.*;

public class TestStudent {

	public static void main(String[] args) {
		
		List<Student> list = populateStudentList();
		
		System.out.println("Orignal List :");
		list.forEach(System.out::println);
		
		/*
		 * Map<String, Student> map = mapFromList(list);
		 * System.out.println("Map from List");
		 * map.forEach((k,v)->System.out.println(k+" "+v));
		 */
		System.out.println("Sorted List as per GPA :");
		list.stream()
			.sorted((o1,o2)->((Double)o1.getGpa()).compareTo(o2.getGpa()))
			.forEach(System.out::println);

	}

}
