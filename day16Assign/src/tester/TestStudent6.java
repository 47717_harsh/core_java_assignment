package tester;

import static utils.CollectionUtils.populateStudentList;

import java.util.List;
import java.util.Scanner;

import com.app.student_info.Student;
import com.app.student_info.Subjects;

public class TestStudent6 {

	public static void main(String[] args) {
		
		try(Scanner sc = new Scanner(System.in))
		{
			List<Student> list = populateStudentList();
			
			System.out.println("Enter Subject");
			Subjects subject = Subjects.valueOf(sc.next().toUpperCase());
			list.stream()
				.filter(i->i.getSubject() == subject)
				.filter(i->i.getGpa() > 5)
				.sorted((o1,o2)->((Double)o2.getGpa()).compareTo(o1.getGpa()))
				.limit(3)
				.forEach(System.out::println);   
		}
		

	}

}
