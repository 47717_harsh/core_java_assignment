package tester;

import static utils.CollectionUtils.populateStudentList;

import java.util.List;
import java.util.Scanner;

import com.app.student_info.Student;
import com.app.student_info.Subjects;

public class TestStudent4 {

	public static void main(String[] args) {
		
		try(Scanner sc = new Scanner(System.in))
		{
			List<Student> list = populateStudentList();
			System.out.println("Enter Subject :");
			
			Subjects subject = Subjects.valueOf(sc.next().toUpperCase());
			
			list.stream()
				.filter(i->i.getSubject() == subject)
				.filter(i->i.getGpa() < 5)
				.forEach(System.out::println);
			
			System.out.println("Enter Subject :");
			Subjects subject1 = Subjects.valueOf(sc.next().toUpperCase());
			long count = list.stream()
				.filter(i->i.getSubject() == subject1)
				.filter(i->i.getGpa() > 7.5)
				.count();
			System.out.println("Count of Distinction(gpa > 7.5) :"+count);
		}
		

	}

}
