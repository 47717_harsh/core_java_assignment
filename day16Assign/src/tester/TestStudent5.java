package tester;

import static utils.CollectionUtils.mapFromList;
import static utils.CollectionUtils.populateStudentList;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.app.student_info.Student;

public class TestStudent5 {

	public static void main(String[] args) {
		
		try(Scanner sc = new Scanner(System.in))
		{
			List<Student> list = populateStudentList();
			Map<String, Student> map = mapFromList(list);
			System.out.println("Display Map Using forEach :");
			map.forEach((k,v)->System.out.println(k+": "+v));
			
			System.out.println("Display Map in Sorted as per roll no :");
			map.values().stream()
					.sorted((o1,o2)->o1.getRollNo().compareTo(o2.getRollNo()))
					.forEach(System.out::println);
			
			System.out.println("Display Map in Sorted as per DOB :");
			map.values().stream()
					.sorted((o1,o2)->o1.getDob().compareTo(o2.getDob()))
					.forEach(System.out::println);
			
			System.out.println("Display Map in Sorted as per roll no(desc) :");
			map.values().stream()
					.sorted((o1,o2)->o2.getRollNo().compareTo(o1.getRollNo()))
					.forEach(System.out::println);
		}
		

	}

}
