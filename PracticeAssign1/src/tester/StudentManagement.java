package tester;

import java.util.List;
import java.util.Scanner;
import static utils.ValidationRules.*;
import static utils.StudentUtils.*;

import com.core.students.Courses;
import com.core.students.Student;

public class StudentManagement {

	static void menu() {
		System.out.println("========== Student Management ==========");
		System.out.println("1. Display All");
		System.out.println("2. Update Student Details");
		System.out.println("3. Cancle Student Admission");
		System.out.println("4. EXIT");
	}

	public static void main(String[] args) {

		List<Student> studentList = populateStudents();
		boolean exit = false;

		try (Scanner sc = new Scanner(System.in)) {
			while (!exit) {
				menu();
				try {
					switch (sc.nextInt()) {
					case 1:
						System.out.println("Enter Course Name");
						Courses name = Courses.valueOf(sc.next().toUpperCase());
						studentList.stream()
								.filter(s -> s.getCourseName() == name)
								.forEach(System.out::println);		
						break;
					case 2:
						System.out.println("Enter Id");
						int id = sc.nextInt();
						Student s1 = getStudentById(studentList,id);
						System.out.println("Enter new marks and new phone no.");
							s1.setMarks(sc.nextDouble());
							s1.setPhoneNo(sc.next());
						break;
					case 3:
						System.out.println("Enter Email");
						int index = getStudentByEmail(studentList,sc.next());
						studentList.remove(index);
						break;
					case 4:
						exit = true;
						break;

					default:
						break;
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

}
