package com.core.students;

public class Student {

	private String name;
	private int id;
	private String email;
	private double marks;
	private String phoneNo;
	private Courses courseName;

	public Student(int id,String name, String email, double marks, String phoneNo, Courses courseName) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.marks = marks;
		this.phoneNo = phoneNo;
		this.courseName = courseName;
	}

	public Student(int id) {
		this.id = id;
	}

	public Student(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", id=" + id + ", email=" + email + ", marks=" + marks + ", phoneNo=" + phoneNo
				+ ", courseName=" + courseName + "]";
	}

	
	@Override
	public boolean equals(Object obj) {
		Student student = (Student)obj;
		if(obj instanceof Student)
			return (this.id == student.id || this.email.equals(student.email));
		return false;
	}

	public double getMarks() {
		return marks;
	}

	public void setMarks(double marks) {
		this.marks = marks;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public int getId() {
		return id;
	}

	public String getEmail() {
		return email;
	}

	public Courses getCourseName() {
		return courseName;
	}
	
	
}
