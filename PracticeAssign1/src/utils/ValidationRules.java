package utils;

import java.util.List;

import com.core.students.Student;

import custom_exception.StudentNotFoundException;

public class ValidationRules {
	
	public static Student getStudentById(List<Student> list,int id) throws StudentNotFoundException
	{
		Student s = new Student(id);
		for(Student s1 : list)
			if(s1.equals(s))
				return s1;
		
		throw new StudentNotFoundException("Enter Valid Student ID");
	}
	
	public static int getStudentByEmail(List<Student> list,String email) throws StudentNotFoundException
	{
		Student s1 = new Student(email);
		int index = list.indexOf(s1);
		if(index != -1)
			return index;
		
		throw new StudentNotFoundException("Invalid Email Address!!");
	}
}
