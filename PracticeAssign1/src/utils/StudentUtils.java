package utils;

import java.util.ArrayList;
import java.util.List;

import com.core.students.Courses;
import com.core.students.Student;

public class StudentUtils {
	
	public static List<Student> populateStudents()
	{
		List<Student> list = new ArrayList<>();
		//int id,String name, String email, double marks, String phoneNo, Coursrs courseName
		list.add(new Student(101,"harsh","harsh@gmail.com",78.88,"9993019555",Courses.JAVA));
		list.add(new Student(102,"yug","yug@gmail.com",88.88,"1234567890",Courses.REACT));
		list.add(new Student(103,"chinmay","chinu@gmail.com",90,"1223567890",Courses.ANGULAR));
		list.add(new Student(104,"adi","adi@gmail.com",79,"1234567890",Courses.JAVA));
		list.add(new Student(105,"viplav","bhagus@gmail.com",77.68,"1234567890",Courses.DOTNET));
		return list;
	}
}
