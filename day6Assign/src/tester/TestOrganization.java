package tester;

import java.util.Scanner;

import com.app.org.Emp;
import com.app.org.Mgr;
import com.app.org.Worker;

public class TestOrganization {

	public static void menu() {
		System.out.println("=============== Organization ================");
		System.out.println("1. Hire Manager");
		System.out.println("2. Hire Worker");
		System.out.println("3. Display Info of all Employees");
		System.out.println("4. Update Basic Salary");
		System.out.println("5. Exit");
		System.out.println("Select Choice");
	}

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter how many employees :");
		Emp[] empList = new Emp[sc.nextInt()];

		boolean exit = false;
		int counter = 0;
		while (exit != true) {
			menu();

			switch (sc.nextInt()) {
			case 1:
				if (counter >= 0 && counter < empList.length) {
					System.out.println("Enter Manager Details(name,deptID,basicSal,performanceBonus) :");
					empList[counter++] = new Mgr(sc.next(), sc.next(), sc.nextDouble(), sc.nextDouble());
				} else
					System.out.println("Sorry No Space!!");
				break;
			case 2:
				if (counter >= 0 && counter < empList.length) {
					System.out.println("Enter Worker Details(name,deptID,basicSal,hoursWorked,hourlyRate) :");
					empList[counter++] = new Worker(sc.next(), sc.next(), sc.nextDouble(), sc.nextDouble(),
							sc.nextDouble());
				} else
					System.out.println("Sorry No Space!!");
				break;
			case 3:

				for (Emp e : empList) {
					if (e != null) {
						System.out.println(e.toString() + " Net Salary :" + e.computeNetSalary());
						if (e instanceof Mgr) {
							((Mgr) e).getPerformanceBonus();
						} else {
							((Worker) e).getHourlyRate();
						}
					}
				}
				break;
			case 4:
				System.out.println("Enter Employee Id and updated Salary :");
				int index = sc.nextInt()-100;
				double sal = sc.nextDouble();
				if(index >=0 && index <counter)
				{
					Emp e1 = empList[index];
					e1.setBasicSal(sal+e1.getBasicSalary());
				}
				else
					System.out.println("Invalid Employee ID!!");
				break;
			case 5:
				System.out.println("Bye!");
				exit = true;
				break;
			}
		}

		sc.close();

	}

}
