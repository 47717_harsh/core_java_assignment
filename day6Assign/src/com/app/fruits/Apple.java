package com.app.fruits;

public class Apple extends Fruit{
	
	public Apple(String color,double weight,boolean fresh)
	{
		super(color,"Apple",weight,fresh);
	}
	
	@Override
	public String taste()
	{
		return ("Sweet and Sour");
	}
	
	public void jam()
	{
		System.out.println(super.getName()+" : making jam!");
	}

}
