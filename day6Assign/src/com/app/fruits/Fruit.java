package com.app.fruits;

public abstract class Fruit {

	private String color;
	private String name;
	private double weight;
	private boolean fresh;

	public Fruit(String color, String name, double weight, boolean fresh) {
		this.color = color;
		this.name = name;
		this.weight = weight;
		this.fresh = fresh;
	}

	public String toString() {
		return ("Name : " + name + " color : "+ color+ " weight : " + weight+" isFresh : "+fresh);
	}

	public abstract String taste();

	public String getName() {
		return name;
	}

	public String getColor() {
		return color;
	}

	public double getWeight() {
		return weight;
	}

	public void setFresh(boolean fresh) {
		this.fresh = fresh;
	}
}
