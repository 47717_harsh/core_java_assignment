package com.app.fruits;

public class Orange extends Fruit{
	
	public Orange(String color,double weight,boolean fresh)
	{
		super(color,"Orange",weight,fresh);
	}
	
	@Override
	public String taste()
	{
		return "Sour";
	}
	
	public void juice()
	{
		System.out.println(getName()+" weight : "+getWeight()+" : extracting juice!");
	}

}
