package com.app.org;

public abstract class Emp {
	
	private int empId;
	private String empName;
	private String deptId;
	private double basicSal;
	private static int idGenerator;
	
	static {
		idGenerator = 100;
	}
	
	public Emp(String empName,String deptId,double basicSal)
	{
		this.empId = idGenerator++;
		this.empName = empName;
		this.deptId = deptId;
		this.basicSal = basicSal;
	}
	
	public int getEmpId() {
		return empId;
	}

	public void setBasicSal(double basicSal) {
		this.basicSal = basicSal;
	}

	public String toString()
	{
		return ("empId: "+empId+" name: "+empName+" deptId: "+deptId+" basic salary: "+basicSal);
	}
	
	public double getBasicSalary()
	{
		return basicSal;
	}
	public abstract double computeNetSalary();

}
