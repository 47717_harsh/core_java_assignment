package com.app.org;

public class Worker extends Emp{
	
	private double hoursWorked;
	private double hourlyRate;
	
	public Worker(String name,String deptId,double basic,double hrsWorked,double hrlyRate)
	{
		super(name,deptId,basic);
		this.hoursWorked = hrsWorked;
		this.hourlyRate = hrlyRate;
	}
	
	public String toString()
	{
		return (super.toString()+" hoursWorked: "+hoursWorked+" hourlyRate: "+hourlyRate);
	}
	
	public double getHourlyRate()
	{
		return hourlyRate;
	}
	
	@Override
	public double computeNetSalary() {
		
		return (super.getBasicSalary()+(hoursWorked*hourlyRate));
	}

}
