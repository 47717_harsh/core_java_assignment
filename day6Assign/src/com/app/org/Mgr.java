package com.app.org;

public class Mgr extends Emp {
	
	private double performanceBonus;
	
	public Mgr(String name,String deptId,double basic,double perBonus)
	{
		super(name,deptId,basic);
		
		this.performanceBonus = perBonus;
	}
	
	public String toString()
	{
		return (super.toString()+" performanceBonus: "+this.performanceBonus);
	}
	
	public double getPerformanceBonus()
	{
		return this.performanceBonus;
	}

	@Override
	public double computeNetSalary() {
		
		return super.getBasicSalary()+performanceBonus;
	}

}
