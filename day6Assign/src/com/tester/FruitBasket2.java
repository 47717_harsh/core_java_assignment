package com.tester;

import java.util.Scanner;

import com.app.fruits.Apple;
import com.app.fruits.Fruit;
import com.app.fruits.Mango;
import com.app.fruits.Orange;

public class FruitBasket2 {

	public static void menu()
	{
		System.out.println("==================== Select Option ===================");
		System.out.println("1. Add Fruit");
		System.out.println("2. Display All Fruits in Basket");
		System.out.println("3. Display Details of all fresh fruit in basket");
		System.out.println("4. Mark a fruit as stale");
		System.out.println("5. Mark all sour fruit as stale");
		System.out.println("6. Invoke fruit specific functionality(pulp/juice/jam)");
		System.out.println("0. EXIT");
	}
	public static void fruitsMenu()
	{
		System.out.println("============ Select Fruits =============");
		System.out.println("1. Mango");
		System.out.println("2. Orange");
		System.out.println("3. Apple");
		System.out.println("0. Done");
	}
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Size of the Basket : ");
		Fruit[] fruitBasket = new Fruit[sc.nextInt()];
		boolean exit = false;
		int counter = 0;
		
		while(exit != true)
		{
			menu();
			
			switch(sc.nextInt())
			{
			case 1:
				boolean done = false;
				while(done != true)
				{
					fruitsMenu();
					
					switch(sc.nextInt())
					{
					case 1:
						break;
					case 2:
						break;
					case 3:
						break;
					case 0:
						System.out.println("Fruits Added");
						done = true;
						break;
					default :
						System.out.println("Invalid choice");
					}
				}
				break;
			case 2:
				for(Fruit f : fruitBasket)
				{
					if(f != null)
					System.out.println(f.getName());
				}
				break;
			case 3:
				for(Fruit f : fruitBasket)
				{
					if(f != null)
					System.out.println(f.toString() + " Taste : "+f.taste());
				}
				break;
			case 4:
				System.out.println("Enter index to mark stale");
				int index = sc.nextInt();
				if(index >=0 && index < counter)
				{
					fruitBasket[index].setFresh(false);
				}
				else
				{
					System.out.println("Invalid Index!!");
				}
				break;
			case 5:
				for(Fruit f : fruitBasket)
				{
					if( f!= null && (f.getName() == "Apple" || f.getName() == "Orange"))
					{
						f.setFresh(false);
					}
				}
				break;
			case 6:
				System.out.println("Enter Index :");
				int ind = sc.nextInt();
				if(ind >=0 && ind < counter)
				{
					Fruit f = fruitBasket[ind];
					if(f instanceof Mango)
						((Mango)f).pulp();
					else if(f instanceof Orange)
						((Orange)f).juice();
					else
						((Apple)f).jam();
				}
				else
				{
					System.out.println("Invalid Index!!");
				}
				break;
			case 0:
				exit = true;
				break;
			default :
				System.out.println("Please Enter Valid Choice");
			}
		}
		
		sc.close();

	}


}
