import java.util.Scanner;

class Assign4
{
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		System.out.println("How many Values you want to enter");
		int N = sc.nextInt();
		int choice = 0;

		double arr[] = new double[N];

		for(int i=0;i<N;i++)
		{
			arr[i] = sc.nextDouble();
		}
		for(int i=0;i<N;i++)
		{
			System.out.print(arr[i] + " ");
		}

		do
		{
			System.out.println();
			System.out.println("===========================================");
			System.out.println("1. Double The Num");
			System.out.println("2. Square The Num");
			System.out.println("3. Square Root The Num");
			System.out.println("4. EXIT");
			choice = sc.nextInt();

			switch(choice)
			{
				case 1:

					System.out.println("--------------------------------");
					System.out.println("Double :");
					for(int i=0;i<N;i++)
					{
						System.out.print(arr[i]*2 + " ");
					}
					break;
				case 2:

					System.out.println("--------------------------------");
					System.out.println("Square :");
					for(int i=0;i<N;i++)
					{
						System.out.print(arr[i]*arr[i] + " ");
					}
			
					break;
				case 3:

					System.out.println("--------------------------------");
					System.out.println("Square root :");
					for(int i=0;i<N;i++)
					{
						System.out.print(Math.sqrt(arr[i]) + " ");
					}
					
					break;
				case 4:
					break;
				default :
						System.out.print("Please Enter Valid Choice");

			}


		}while(choice != 4);
	}
}