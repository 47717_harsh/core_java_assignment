package custom_exception;

public class BuddyHandlingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BuddyHandlingException(String message) {
		super(message);
	}
	

}
