package utils;

import java.time.LocalDate;
import java.util.Map;

import com.core.buddies.Buddy;

import custom_exception.BuddyHandlingException;

public class ValidationRules {

	public static void validateEmail(Map<String,Buddy> map,String email) throws BuddyHandlingException
	{
		if(map.containsKey(email))
			throw new BuddyHandlingException("Buddy Already Exists");
	}
	
	public static LocalDate validateDob(String date) throws BuddyHandlingException
	{
		LocalDate dob = LocalDate.parse(date);
		if(dob.isAfter(LocalDate.now()))
			throw new BuddyHandlingException("Invalid Dob");
		return dob;
	}
	
	public static Buddy validateAndCreate(Map<String,Buddy> map,String email, String name, String phoneNo, String dob, String city) throws BuddyHandlingException
	{
		validateEmail(map,email);
		LocalDate dateOfBirth = validateDob(dob);
		return new Buddy(email,name,phoneNo,dateOfBirth,city);
	}
}
