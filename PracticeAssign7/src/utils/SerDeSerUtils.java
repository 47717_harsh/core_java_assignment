package utils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.util.Map;

import com.core.buddies.Buddy;

public interface SerDeSerUtils {

	static void storeBuddyDetails(Map<String,Buddy> map,String fileName) throws FileNotFoundException, IOException
	{
		try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName)))
		{
			out.writeObject(map);
		}
	}
	
	static void storeDetailsInTextFile(Map<String,Buddy> map,String fileName) throws IOException
	{
		try(PrintWriter pw = new PrintWriter(new FileWriter(fileName)))
		{
			map.forEach((k,v) -> pw.println(k+" --> "+v));
		}
	}
}
