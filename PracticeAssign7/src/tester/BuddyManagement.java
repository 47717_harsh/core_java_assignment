package tester;

import static utils.ValidationRules.validateAndCreate;
import static utils.ValidationRules.validateDob;
import static utils.SerDeSerUtils.*;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.core.buddies.Buddy;

import custom_exception.BuddyHandlingException;

public class BuddyManagement {

	static void menu() {
		System.out.println("========== Buddy Management ==========");
		System.out.println("1. Add New Buddy");
		System.out.println("2. Search by email");
		System.out.println("3. Delete by email");
		System.out.println("4. Display All");
		System.out.println("5. Display by DOB");
		System.out.println("6. EXIT");
	}
	
	public static void main(String[] args) {
		
		Map<String,Buddy> buddies = new HashMap<String,Buddy>();
		try(Scanner sc = new Scanner(System.in))
		{
			boolean exit = false;
			
			while(!exit)
			{
				menu();
				
				try {
					
					switch (sc.nextInt()) {
					case 1:
						System.out.println("Enter Buddies Detail(email,name,phoneNo,dob(yyyy-mm-dd),city)");
						Buddy b1 = validateAndCreate(buddies,sc.next(),sc.next(),sc.next(),sc.next(),sc.next());
						buddies.put(b1.getEmail(), b1);
						System.out.println("Added Successfully!!");
						break;
					case 2:
						System.out.println("Enter email :");
						b1 = buddies.get(sc.next());
						if(b1 == null)
							throw new BuddyHandlingException("Buddy Not Found");
						System.out.println(b1); 
						break;
					case 3:
						System.out.println("Enter email :");
						b1 = buddies.remove(sc.next());
						if(b1 == null)
							throw new BuddyHandlingException("Buddy Not Found");
						System.out.println("Successfully Deleted");
						break;
					case 4:
						System.out.println("Buddies :");
						buddies.forEach((k,v)->System.out.println(v));
						break;
					case 5:
						System.out.println("Enter DOB(yyyy-mm-dd)");
						LocalDate dob = validateDob(sc.next());
						buddies.values().stream()
							.filter(i -> i.getDob().equals(dob))
							.forEach(System.out::println);
						break;
					case 6:
						System.out.println("Enter FileName :");
						storeBuddyDetails(buddies,sc.next());
						storeDetailsInTextFile(buddies,"file1.txt");
						System.out.println("Saved!!");
						exit = true;
						break;
					default:
						break;
					}
					
				} catch (Exception e) {
					System.out.println(e);
				}
				sc.nextLine();
			}
		}

	}

}
