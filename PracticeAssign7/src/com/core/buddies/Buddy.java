package com.core.buddies;

import java.io.Serializable;
import java.time.LocalDate;

public class Buddy implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3923332969576491281L;
	private String email;
	private String name;
	private String phoneNo;
	private LocalDate dob;
	private String city;
	
	public Buddy(String email, String name, String phoneNo, LocalDate dob, String city) {
		super();
		this.email = email;
		this.name = name;
		this.phoneNo = phoneNo;
		this.dob = dob;
		this.city = city;
	}

	@Override
	public String toString() {
		return "Buddy [email=" + email + ", name=" + name + ", phoneNo=" + phoneNo + ", dob=" + dob + ", city=" + city
				+ "]";
	}

	public String getEmail() {
		return email;
	}

	public String getName() {
		return name;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public LocalDate getDob() {
		return dob;
	}

	public String getCity() {
		return city;
	}
	
}
