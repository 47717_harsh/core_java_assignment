package generics;

import static generics.GenericUtils.findMaxOfAynType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

public class Test1 {

	public static void main(String[] args) {
		
		ArrayList<Integer> list = new ArrayList<>(Arrays.asList(1,16,55,15,45,80,5));
		System.out.println(findMaxOfAynType(list));
		LinkedList<Double> dlist = new LinkedList<>(Arrays.asList(1.2,160.5,55.8,15.0,45.9,80.2,5.9));
		System.out.println(findMaxOfAynType(dlist));
		Vector<Float> flist = new Vector<>(Arrays.asList(1.2f,160.5f,555.8f,15.0f,45.9f));
		System.out.println(findMaxOfAynType(flist));
		Byte[] bytes= {100,27,39,40,50};
		List<Byte> l1=Arrays.asList(bytes);
		ArrayList<Byte> blist = new ArrayList<>(l1);
		System.out.println(findMaxOfAynType(blist));
		LinkedList<Float> flinkedlist = new LinkedList<>(Arrays.asList(1.2f,16.5f,55.8f,150.0f,45.9f));
		System.out.println(findMaxOfAynType(flinkedlist));
	}

}
