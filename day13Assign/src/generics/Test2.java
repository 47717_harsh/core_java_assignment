package generics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Vector;

import static generics.GenericUtils.*;
public class Test2 {

	public static void main(String[] args) {
		
		ArrayList<Worker> list = new ArrayList<>(Arrays.asList(new Worker(1000),new Worker(2000),new Worker(5000)));
		ArrayList<Emp> empList = new ArrayList<>(Arrays.asList(new Emp(10),new Emp(20),new Emp(30),new Emp(54)));
		System.out.println("Workers List : "+list);
		copyAllListRef(empList,list);
		Collections.copy(empList,list);
		System.out.println("Emp List : "+empList);
		Vector<Mgr> flist = new Vector<>(Arrays.asList(new Mgr(10.9),new Mgr(20.3),new Mgr(50.50)));
		System.out.println("Mgr List : "+flist);
		copyAllListRef(empList,flist);
		System.out.println("Emp List : "+empList);
		LinkedList<SalesMgr> sMgr = new LinkedList<>(Arrays.asList(new SalesMgr(236),new SalesMgr(36)));
		System.out.println("Salse Mgr List : "+sMgr);
		copyAllListRef(empList,sMgr);
		System.out.println("Emp List : "+empList);
	}

}
