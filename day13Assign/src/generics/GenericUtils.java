package generics;

import java.util.List;

public class GenericUtils {
	
	public static<T extends Number & Comparable<T>> T findMaxOfAynType(List<T> list) 
	{
		T max = list.get(0);
		for(T n : list)
		{
			if(n.compareTo(max) > 0)
				max = n;
		}
		return max;
	}
	//                                   List<? super Worker>,List<? extends Worker>
	public static<T> void copyAllListRef(List<? super T> dest,List<? extends T> src)
	{
		for(int i=0;i<src.size();i++)
		{
			dest.set(i, src.get(i));
		}
	}
}
