package tester;

import java.util.Scanner;

import com.app.org.Mgr;
import com.app.org.Worker;

public class TestOrganization1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Managers Detail : ");
		Mgr manager1 = new Mgr(sc.next(),sc.next(),sc.nextDouble(),sc.nextDouble());
		System.out.println("Enter Workers Detail : ");
		Worker worker1 = new Worker(sc.next(),sc.next(),sc.nextDouble(),sc.nextDouble(),sc.nextDouble());
		
		System.out.println("Managers Detail :"+manager1.toString());
		System.out.println("Workers Detail :"+worker1.toString());
		
		sc.close();

	}

}
