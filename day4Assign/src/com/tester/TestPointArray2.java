package com.tester;

import java.util.Scanner;

import com.app.geometry.Point2D;


public class TestPointArray2 {
	
	public static void menu()
	{
		System.out.println("---------- MENU -------------");
		System.out.println("1. Plot a new point");
		System.out.println("2. Display all points");
		System.out.println("3. Test equality of 2 points");
		System.out.println("4. Calculate Distance");
		System.out.println("5. Exit");
		System.out.println("Enter Choice");
	}

	public static void main(String[] args) {
		
		int choice = 0;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter How many points to plot");
		Point2D[] points = new Point2D[sc.nextInt()];
		
		do {
			
			menu();
			choice = sc.nextInt();
			
			switch(choice)
			{
			case 1:
				System.out.println("Enter Index");
				int index = sc.nextInt();
				if(index < 0 || index > points.length-1)
				{
					System.out.println("Please Enter Valid Index");
				}
				else
				{
					System.out.println("Enter X and Y coordinates :");
					points[index] = new Point2D(sc.nextDouble(),sc.nextDouble());
				}
				
				break;
			case 2:
				
				System.out.println("All Points : ");
				for(Point2D i : points)
				{
					if(i != null)
						System.out.println(i.getDetails());
					else
						continue;
				}
				break;
			case 3:
				
				System.out.println("Enter two indexes for equality check :");
				int index1 = sc.nextInt();
				int index2 = sc.nextInt();
				if((index1 <0 || index1 >= points.length || index2 < 0 || index2 >= points.length) || points[index1]==null || points[index2]== null)
				{
					System.out.println("Please enter valid index!!");
				}
				else
				{
					System.out.println("Is Equal : "+points[index1].isEqual(points[index2]));
				}
				
				break;
			case 4:
				
				System.out.println("Enter Start and End Point index : ");
				int start = sc.nextInt();
				int end = sc.nextInt();
				if(start <0 || start >= points.length || end < 0 || end >= points.length || points[start]==null || points[end]== null)
				{
					System.out.println("Please enter valid index!!");
				}
				else
				{
					System.out.println("Distance : "+points[end].calculateDistance(points[start]));
				}
				break;
			case 5:
				System.out.println("Bye!");
				break;
			default :
				System.out.println("Please Choose Valid Choice");
				
			}
			
			
		}while(choice != 5); 
		
		sc.close();

	}

}
