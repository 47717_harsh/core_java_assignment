package com.app.geometry;

public class Point2D {

	private double x;
	private double y;
	
	public Point2D(double x,double y)
	{
		this.x = x;
		this.y = y;
	}
	
	public String getDetails()
	{
		return ("Coordinates : "+x+" "+y);
	}
	
	public boolean isEqual(Point2D otherPoint) 
	{
		return (this.x == otherPoint.x && this.y == otherPoint.y);
	}
	
	public double calculateDistance(Point2D newPoint)
	{
		return Math.sqrt((Math.pow(this.x-newPoint.x,2))+(Math.pow(this.y-newPoint.y,2)));
	}
}
