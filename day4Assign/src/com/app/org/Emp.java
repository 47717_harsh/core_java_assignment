package com.app.org;

public class Emp {
	
	private int empId;
	private String empName;
	private String deptId;
	private double basicSal;
	private static int counter = 0;
	
	public static int generateEmpId()
	{
		return ++counter;
	}
	
	public Emp(String empName,String deptId,double basicSal)
	{
		this.empId = generateEmpId();
		this.empName = empName;
		this.deptId = deptId;
		this.basicSal = basicSal;
	}
	
	public String toString()
	{
		return (this.empId+" "+this.empName+" "+this.deptId+" "+this.basicSal);
	}

}
