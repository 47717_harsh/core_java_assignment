package com.app.org;

public class Mgr extends Emp {
	
	private double performanceBonus;
	
	public Mgr(String name,String deptId,double basic,double perBonus)
	{
		super(name,deptId,basic);
		
		this.performanceBonus = perBonus;
	}
	
	public String toString()
	{
		return (super.toString()+" "+this.performanceBonus);
	}
	
	public double getPerBonus()
	{
		return this.performanceBonus;
	}

}
