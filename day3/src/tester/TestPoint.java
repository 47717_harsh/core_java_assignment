package tester;

import java.util.Scanner;

import com.app.geometry.point2D;

public class TestPoint {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter x and y coordinates : ");
		
		point2D p1 = new point2D(sc.nextDouble(),sc.nextDouble());
		
		System.out.println("Enter x and y coordinates : ");
		point2D p2 = new point2D(sc.nextDouble(),sc.nextDouble());
		
		System.out.println("Point 1 "+p1.getDetails());
		System.out.println("Point 2 "+p2.getDetails());
		
		if(p1.isEqual(p2))
		{
			System.out.println("Points at same position");
		}
		else
		{
			System.out.println("Distance between two points : "+p1.calculateDistance(p2));
		}
		
		sc.close();

	}

}
