package tester;

import java.util.Scanner;

import com.app.geometry.point2D;

public class TestPointArray {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("How many points to plot : ");
		point2D[] points = new point2D[sc.nextInt()];
		
		
		for(int i=0;i<points.length;i++)
		{
			System.out.println("Enter Coordinates :");
			points[i] = new point2D(sc.nextDouble(),sc.nextDouble());
		}
		
		for(point2D i : points)
		{
			System.out.println(i.getDetails());
		}
		
		sc.close();

	}

}
