package com.app.geometry;

public class point2D {
	
	private double x;
	private double y;
	
	public point2D(double x,double y)
	{
		this.x = x;
		this.y = y;
	}
	
	public String getDetails()
	{
		return ("Coordinates : "+x+" "+y);
	}
	
	public boolean isEqual(point2D otherPoint)
	{
		return (this.x == otherPoint.x && this.y == otherPoint.y);
	}
	
	public double calculateDistance(point2D newPoint)
	{
		return Math.sqrt((Math.pow(this.x-newPoint.x,2))+(Math.pow(this.y-newPoint.y,2)));
	}
}
