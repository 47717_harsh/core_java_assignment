package com.tester;

import java.util.Scanner;
import com.app.fruits.Apple;
import com.app.fruits.Fruit;
import com.app.fruits.Mango;
import com.app.fruits.Orange;

public class FruitBasket {

	public static void menu()
	{
		System.out.println("==================== Select Option ===================");
		System.out.println("1. Add Mango");
		System.out.println("2. Add Orange");
		System.out.println("3. Add Apple");
		System.out.println("4. Display All Fruits in Basket");
		System.out.println("5. Display Details of all fresh fruit in basket");
		System.out.println("6. Mark a fruit as stale");
		System.out.println("7. Mark all sour fruit as stale");
		System.out.println("8. Invoke fruit specific functionality(pulp/juice/jam)");
		System.out.println("9. EXIT");
	}
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Size of the Basket : ");
		Fruit[] fruitBasket = new Fruit[sc.nextInt()];
		boolean exit = false;
		int counter = 0;
		
		while(exit != true)
		{
			menu();
			
			switch(sc.nextInt())
			{
			case 1:
				if(counter >= 0 && counter < fruitBasket.length)
				{
					System.out.println("Enter color and weight of Mango");
					fruitBasket[counter++] = new Mango(sc.next(),sc.nextDouble(),true);
				}
				else
					System.out.println("Basket Full!!");
				break;
			case 2:
				if(counter >= 0 && counter < fruitBasket.length)
				{
					System.out.println("Enter color and weight of Orange");
					fruitBasket[counter++] = new Orange(sc.next(),sc.nextDouble(),true);
				}
				else
					System.out.println("Basket Full!!");
				break;
			case 3:
				if(counter >= 0 && counter < fruitBasket.length)
				{
					System.out.println("Enter color and weight of Apple");
					fruitBasket[counter++] = new Apple(sc.next(),sc.nextDouble(),true);
				}
				else
					System.out.println("Basket Full!!");
				break;
			case 4:
				for(Fruit f : fruitBasket)
				{
					if(f != null)
					System.out.println(f.getName());
				}
				break;
			case 5:
				for(Fruit f : fruitBasket)
				{
					if(f != null)
					System.out.println(f.toString() + " Taste : "+f.taste());
				}
				break;
			case 6:
				System.out.println("Enter index to mark stale");
				int index = sc.nextInt();
				if(index >=0 && index < counter)
				{
					fruitBasket[index].setFresh(false);
				}
				else
				{
					System.out.println("Invalid Index!!");
				}
				break;
			case 7:
				for(Fruit f : fruitBasket)
				{
					if( f!= null && (f.getName() == "Apple" || f.getName() == "Orange"))
					{
						f.setFresh(false);
					}
				}
				break;
			case 8:
				System.out.println("Enter Index :");
				int ind = sc.nextInt();
				if(ind >=0 && ind < counter)
				{
					Fruit f = fruitBasket[ind];
					if(f instanceof Mango)
						((Mango)f).pulp();
					else if(f instanceof Orange)
						((Orange)f).juice();
					else
						((Apple)f).jam();
				}
				else
				{
					System.out.println("Invalid Index!!");
				}
				break;
			case 9:
				exit = true;
				break;
			}
		}
		
		sc.close();

	}

}
