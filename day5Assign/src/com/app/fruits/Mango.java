package com.app.fruits;

public class Mango extends Fruit{
	
	public Mango(String color,double weight,boolean fresh)
	{
		super(color,"Mango",weight,fresh);
	}
	
	public void pulp()
	{
		System.out.println(getName()+" color : "+getColor()+" : Creating Pulp!");
	}
	
	public String taste()
	{
		return ("Sweet");
	}
	
}
