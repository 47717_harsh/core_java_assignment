package utils;

import static com.app.customer.Customer.sdf;

import java.text.ParseException;
import java.util.Date;

import com.app.customer.Customer;
import com.app.customer.CustomerType;

import custom_exception.CustomerHandlingException;

public class ValidationRules {
	
	public static void validateEmail(String email) throws CustomerHandlingException
	{
		String regex = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.+-]+\\.com$";
		if(!email.matches(regex))
		{
			throw new CustomerHandlingException("Invalid email!!");
		}
	}
	public static void validatePassword(String password) throws CustomerHandlingException
	{
		if(password.length()<4 || password.length()>10)
		{
			throw new CustomerHandlingException("Invalid Password!!");
		}
	}
	public static Date validateDob(String dob) throws ParseException,CustomerHandlingException
	{
		Date d1 = sdf.parse(dob);
		Date begin = sdf.parse("01/01/1995");
		if(!d1.before(begin))
		{
			throw new CustomerHandlingException("Invalid Date!!");
		}
		return d1;
	}
	public static void validateDuplicateCustomer(Customer[] customerList,String email) throws CustomerHandlingException
	{
		Customer newCustomer = new Customer(email);
		for(Customer c : customerList)
		{
			if(c!=null)
			{
				if(c.equals(newCustomer))
				{
					throw new CustomerHandlingException("Email Id already exists");
				}
			}
		}
	}
	
	public static Customer ValidateAndCreateNew(String name, String email, String password, double registrationAmount, String dob,
			String type,Customer[] customerList) throws CustomerHandlingException, ParseException
	{
		validateDuplicateCustomer(customerList,email);
		validateEmail(email);
		Date newDob = validateDob(dob);
		validatePassword(password);
		return new Customer(name,email,password,registrationAmount,newDob,CustomerType.valueOf(type.toUpperCase()));
	}

}
