package tester;

import java.util.Scanner;
import static utils.ValidationRules.ValidateAndCreateNew;
import com.app.customer.Customer;
import com.app.customer.CustomerType;

import custom_exception.CustomerHandlingException;

public class Tester1 {
	
	public static void menu() {
		System.out.println("============== CUSTOMER =============");
		System.out.println("1. Add Customer");
		System.out.println("2. Display All Customer Details");
		System.out.println("3. Exit");
		System.out.println("Select Choice");
	}
	public static void customerTypeMenu()
	{
		System.out.println("------ Customer Type ------");
		System.out.println("0. SILVER");
		System.out.println("1. GOLD");
		System.out.println("2. DIAMOND");
		System.out.println("3. PLATINUM");
	}

	public static void main(String[] args) {
		
		try(Scanner sc = new Scanner(System.in))
		{
			System.out.println("Enter No of Customers: ");
			Customer[] customerList = new Customer[sc.nextInt()];
			boolean exit = false;
			int counter = 0;
			
			while(!exit)
			{
				menu();
				try {
					
					switch (sc.nextInt()) {
					case 1:
						if (counter < customerList.length) {
							System.out.println("Enter Customer Details(name,email,password,registrationAmount,dob,Registration Type) :");
							System.out.println("Registration Types Options:");
							for(CustomerType c : CustomerType.values())
							{
								System.out.println(c);
							}
							customerList[counter++] = ValidateAndCreateNew(sc.next(),sc.next(),sc.next(),sc.nextDouble(),sc.next(),sc.next(),customerList);
						}
						else
							throw new CustomerHandlingException("No more space");
						break;
					case 2:
						for(Customer c : customerList)
						{
							if(c!=null)
								System.out.println(c);
						}
						break;
					case 3:
						System.out.println("Bye!");
						exit = true;
						break;

					default:
						break;
					}
					
				}catch(Exception e)
				{
					System.out.println(e.getMessage());
				}
				sc.nextLine();
			}
		}
	}

}
