package com.app.customer;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Customer {

	private String name;
	private String email;
	private String password;
	private double registrationAmount;
	private Date dob;
	private int Id;
	private CustomerType type;
	private static int generateCustomerId;
	public static SimpleDateFormat sdf;
	static {
		generateCustomerId = 100;
		sdf = new SimpleDateFormat("dd/MM/yyyy");
	}

	public Customer(String name, String email, String password, double registrationAmount, Date dob,
			CustomerType type) {
		this.Id = generateCustomerId++;
		this.name = name;
		this.email = email;
		this.password = password;
		this.registrationAmount = registrationAmount;
		this.dob = dob;
		this.type = type;
	}

	public Customer(String email) {
		super();
		this.email = email;
	}

	@Override
	public boolean equals(Object obj) {
		Customer cust = (Customer) obj;
		if (obj instanceof Customer)
			return (email.equals(cust.email));
		return false;
	}

	@Override
	public String toString() {
		return "Customer [name=" + name + ", email=" + email + ", password=" + password + ", registrationAmount="
				+ registrationAmount + ", dob=" + dob + ", Id=" + Id + ", type=" + type + "]";
	}
}
