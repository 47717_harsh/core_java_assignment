package threads;

public class OddPrinterTask extends Thread{
	
	private int begin;
	private int end;
	public OddPrinterTask(int begin, int end,String name) {
		super(name);
		this.begin = begin;
		this.end = end;
		start();
	}
	@Override
	public void run() {
			for (int i = begin; i <= end; i++) {
				if (i % 2 != 0) {
					System.out.println("Odd : " + i);
					try {
						Thread.sleep(250);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
