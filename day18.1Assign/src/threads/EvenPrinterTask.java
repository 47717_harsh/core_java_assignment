package threads;

public class EvenPrinterTask extends Thread{
	
	private int begin;
	private int end;
	public EvenPrinterTask(int begin, int end,String name) {
		super(name);
		this.begin = begin;
		this.end = end;
		start();
	}
	@Override
	public void run() {
		
			for(int i = begin;i<=end;i++)
			{
				if(i%2 == 0)
				{
					System.out.println("Even : "+i);
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
		
	}
