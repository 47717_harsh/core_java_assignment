package threads;

public class PrimePrinterTask extends Thread{
	
	private int begin;
	private int end;
	public PrimePrinterTask(int begin, int end,String name) {
		super(name);
		this.begin = begin;
		this.end = end;
		start();
	}
	@Override
	public void run() {
			for (int i = begin; i <= end; i++) {
				if (isPrime(i)) {
					System.out.println("Prime : " + i);
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	static boolean isPrime(int n) {
		if (n <= 1)
			return false;
		for (int i = 2; i < n; i++)
			if (n % i == 0)
				return false;

		return true;
	}
}


