package tester;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import threads.*;

public class TestThreadUsingExtends {

	public static void main(String[] args) {
		
		System.out.println("Enter Start and End Point :");
		try(Scanner sc = new Scanner(System.in))
		{
			int start = sc.nextInt();
			int end = sc.nextInt();
			
			EvenPrinterTask t1 = new EvenPrinterTask(start, end, "Even");
			OddPrinterTask t2 = new OddPrinterTask(start, end, "Odd");
			PrimePrinterTask t3 = new PrimePrinterTask(start, end, "Prime");
			
			ArrayList<Thread> list = new ArrayList<>(Arrays.asList(t1,t2,t3));
			
			list.forEach(i->{
				try {
					i.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			});
			System.out.println("Main Over....");
		}

	}

}
