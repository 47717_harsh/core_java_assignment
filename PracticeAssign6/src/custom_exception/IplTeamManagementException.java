package custom_exception;

public class IplTeamManagementException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public IplTeamManagementException(String message) {
		super(message);
	}
	

}
