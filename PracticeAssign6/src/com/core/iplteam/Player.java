package com.core.iplteam;

import java.io.Serializable;

public class Player implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3723046241412883955L;
	private String playerName;
	private int id;
	private int ranking;
	private double points;
	
	public Player(String playerName, int id, int ranking, double points) {
		super();
		this.playerName = playerName;
		this.id = id;
		this.ranking = ranking;
		this.points = points;
	}

	public Player(String playerName, double points) {
		this.playerName = playerName;
		this.points = points;
	}

	@Override
	public boolean equals(Object obj) {
		Player p = (Player)obj;
		if(obj instanceof Player) {
			return (playerName.equals(p.playerName) && points == p.points);
		}
		return false;
	}

	@Override
	public String toString() {
		return "Player [id =" + id + ", playerName=" + playerName + ", ranking=" + ranking + ", points=" + points + "]";
	}

	public String getPlayerName() {
		return playerName;
	}

	public int getId() {
		return id;
	}

	public int getRanking() {
		return ranking;
	}

	public double getPoints() {
		return points;
	}
	
	
}
