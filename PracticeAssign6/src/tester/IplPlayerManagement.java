package tester;

import java.util.Map;
import java.util.Scanner;

import com.core.iplteam.Player;

import custom_exception.IplTeamManagementException;

import static utils.SerDeSer.*;
import static utils.ValidationRules.*;

public class IplPlayerManagement {

	static void menu() {
		System.out.println("========== IPL Player Management ==========");
		System.out.println("1. Add Player");
		System.out.println("2. Display IPL Team");
		System.out.println("3. Remove Player");
		System.out.println("4. EXIT");
	}
	
	public static void main(String[] args) {
		
		try(Scanner sc = new Scanner(System.in))
		{
			System.out.println("Enter File Name :");
			String fileName = sc.next();
			
			Map<Integer,Player> players = extractPlayerDetails(fileName);
			boolean exit = false;
			int counter = players.size();
			while(!exit)
			{
				menu();
				
				try {
					
					switch (sc.nextInt()) {
					case 1:
						if(counter >= 7) System.out.println("Maximum 7 players are Allowed");
						else {
							System.out.println("Enter Player Details(playerName,id,ranking,points) :");
							Player p1 = validateAndCreate(players,sc.next(),sc.nextInt(),sc.nextInt(),sc.nextDouble());
							Player p = players.putIfAbsent(p1.getId(), p1);
							if(p != null)
								throw new IplTeamManagementException("Player with this id already exists");
							counter++;
						}
						
						break;
					case 2:
						if(counter < 5 ) {
							System.out.println("Minimum 5 Players Required!!!");
							break;
						}
						System.out.println("Players List");
						players.forEach((k,v) -> System.out.println(v));
						break;
					case 3:
						if(counter <= 5) {
							System.out.println("Cannot Remove : Minimum 5 Players Required!!!");
							break;
						}
						System.out.println("Enter Player Id :");
						Player p1 = players.remove(sc.nextInt());
						if(p1 == null)
							throw new IplTeamManagementException("Player with this id does not exists");
						System.out.println("Player Deleted Successfully!!");
						counter--;
						break;
					case 4:
						storePlayerDetails(players,fileName);
						System.out.println("Successfully Saved !!");
						exit = true;
						break;
					default:
						break;
					}
					
				} catch (Exception e) {
					System.out.println(e);
				}
			}
			
			
		}catch(Exception e) {
			System.out.println(e);
		}

	}

}
