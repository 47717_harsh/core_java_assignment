package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

import com.core.iplteam.Player;

public interface SerDeSer {
	
	static void storePlayerDetails(Map<Integer,Player> map,String fileName) throws FileNotFoundException, IOException
	{
		try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName)))
		{
			out.writeObject(map);
		}
	}
	
	static Map<Integer,Player> extractPlayerDetails(String fileName) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		File f1 = new File(fileName);
		if(f1.exists() && f1.isFile() && f1.canRead())
		{
			try(ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName)))
			{
				@SuppressWarnings("unchecked")
				Map<Integer,Player> map = (Map<Integer, Player>) in.readObject();
				return map;
			}
		}
		return new HashMap<Integer,Player>();
	}
}
