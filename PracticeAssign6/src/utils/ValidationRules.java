package utils;

import java.util.Map;

import com.core.iplteam.Player;

import custom_exception.IplTeamManagementException;

public class ValidationRules {

	public static void validateRanking(int ranking) throws IplTeamManagementException
	{
		if(ranking < 1 || ranking > 5)
			throw new IplTeamManagementException("Ranking must be between 1-5");
	}
	
	public static void validateDuplicatePlayer(Map<Integer,Player> map,String name,double points) throws IplTeamManagementException
	{
		Player newPlayer = new Player(name,points);
		for(Player p : map.values())
		{
			if(p.equals(newPlayer))
				throw new IplTeamManagementException("Duplicate Entry Not Allowed!!");
		}
	}
	
	public static Player validateAndCreate(Map<Integer,Player> map,String playerName, int id, int ranking, double points) throws IplTeamManagementException
	{
		validateRanking(ranking);
		validateDuplicatePlayer(map,playerName,points);
		
		return new Player(playerName,id,ranking,points);
	}
}
