package tester;

import java.util.Map;
import java.util.Scanner;
import static utils.SerDeSerUtils.*;
import static utils.ValidationRules.*;

import com.app.product.Product;

public class ProductManagement {

	static void menu() {
		System.out.println("========== Product Management ==========");
		System.out.println("1. Display Product List");
		System.out.println("2. Add new Product");
		System.out.println("3. Remove by Name");
		System.out.println("4. EXIT");
	}
	
	public static void main(String[] args) {
		
		try(Scanner sc = new Scanner(System.in))
		{
			System.out.println("Enter File Name");
			String fileName = sc.nextLine();
			
			Map<Integer,Product> products = restoreProductDetails(fileName);
			boolean exit = false;
			
			while(!exit)
			{
				menu();
				
				try {
					switch (sc.nextInt()) {
					
					case 1:
						products.forEach((k,v)->System.out.println(v));
						break;
					case 2:
						System.out.println("Enter Product Details(productName,productPrice,expiryDate) ");
						Product p1 = validateAndCreate(products,sc.next(),sc.nextDouble(),sc.next());
						products.put(p1.getId(), p1);
						break;
					case 3:
						System.out.println("Enter Product Name :");
						String pName = sc.next();
						products.values().removeIf(p -> p.getProductName().equals(pName));
						break;
					case 4:
						storeProductDetails(products,fileName);
						System.out.println("Product Details Saved ");
						exit = true;
						break;
					default:
						break;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 

	}

}
