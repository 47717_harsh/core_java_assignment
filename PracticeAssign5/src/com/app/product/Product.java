package com.app.product;

import java.io.Serializable;
import java.time.LocalDate;

public class Product implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3983030358013058875L;
	private int id;
	private String productName;
	private double productPrice;
	private LocalDate expiryDate;
	private static int generateId;
	static {
		generateId = 100;	
		}
	
	public Product(String productName, double productPrice, LocalDate expiryDate) {
		
		this.id = ++generateId;
		this.productName = productName;
		this.productPrice = productPrice;
		this.expiryDate = expiryDate;
	}
	
	public int getId() {
		return id;
	}

	public String getProductName() {
		return productName;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public Product(String productName, double productPrice) {
		this.productName = productName;
		this.productPrice = productPrice;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", productName=" + productName + ", productPrice=" + productPrice + ", expiryDate="
				+ expiryDate + "]";
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		Product pro = (Product)obj;
		if(obj instanceof Product)
			return (productName.equals(pro.productName) && ((Double)productPrice).equals(pro.productPrice));
		return false;
	}
}
