package utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;

import com.app.product.Product;

public interface SerDeSerUtils {
	
	static void storeProductDetails(Map<Integer,Product> map,String fileName) throws FileNotFoundException, IOException
	{
		try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fileName)))
		{
			out.writeObject(map);
		}
	}
	
	@SuppressWarnings("unchecked")
	static Map<Integer,Product> restoreProductDetails(String fileName) throws FileNotFoundException, IOException, ClassNotFoundException
	{
		File f1 = new File(fileName);
		if (f1.exists() && f1.isFile() && f1.canRead()) 
		{
			try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(fileName))) {
				return (Map<Integer, Product>) in.readObject();
			} 
		}
		return new HashMap<Integer,Product>();
	}
}
