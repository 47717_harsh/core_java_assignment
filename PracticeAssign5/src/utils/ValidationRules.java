package utils;

import java.time.LocalDate;
import java.util.Map;

import com.app.product.Product;

import custom_exception.ProductManagementException;

public class ValidationRules {

	public static LocalDate validateExpiryDate(String expDate) throws ProductManagementException
	{
		LocalDate exp = LocalDate.parse(expDate);
		if(exp.isBefore(LocalDate.now()))
			throw new ProductManagementException("Product is Expired!!");
		return exp;
	}
	
	public static void checkForDuplicateProduct(Map<Integer,Product> map,String name,double price) throws ProductManagementException
	{
		Product product = new Product(name,price);
		for(Product p : map.values())
			if(p.equals(product))
				throw new ProductManagementException("Duplicate Product Not allowed!!");
	}
	
	public static Product validateAndCreate(Map<Integer,Product> map,String productName, double productPrice, String expiryDate) throws ProductManagementException
	{
		LocalDate expDate = validateExpiryDate(expiryDate);
		checkForDuplicateProduct(map,productName,productPrice);
		return new Product(productName,productPrice,expDate);
	}
}
