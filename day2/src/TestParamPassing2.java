package params_passing;
class  TestParamPassing2
{
	public static void main(String[] args) 
	{
	//write a static method to update emp sal.
	Emp e1=new Emp(1,"aa",1000);
	System.out.println("orig sal bef  ="+e1.getSal() );// 1000
	testMe(e1,100);
	System.out.println("in main, after updation   ="+e1.getSal());// 1100
	}
	      // copy ref of e1 to e   100
	 static void testMe(Emp e,double inc)
	{
		e.setSal(e.getSal()+inc); // 1000+100 = 1100
		System.out.println("in meth2 sal="+e.getSal()); //1100
	}

}
