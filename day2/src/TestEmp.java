import java.util.Scanner;

class TestEmp
{
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter empId,empName and Salary : ");
		Emp emp1 = new Emp(sc.nextInt(),sc.nextLine(),sc.nextDouble());

		System.out.println("Enter new salary : ");
		emp1.setSal(sc.nextDouble());

		System.out.println("Salary is : "+emp1.getSal());
	}
}