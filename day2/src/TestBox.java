import java.util.Scanner;

class TestBox
{
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter box Dimensions and/or Color(w,d,h,col) : ");
		Box box1 = new Box(sc.nextDouble(),sc.nextDouble(),sc.nextDouble(),sc.nextLine());
		Box box2 = new Box(sc.nextDouble(),sc.nextDouble(),sc.nextDouble(),sc.nextLine());

		if(box1.getComputedVolume() > box2.getComputedVolume())
			System.out.println("Color : "+box1.getColor());
		else
			System.out.println("Color : "+box2.getColor());

		Box newBox = box1.createNewBox(3,-4,-2);
		System.out.println(newBox.getBoxDetails());

		System.out.println(box2.isEqual(box1));
		//System.out.println(box1.isEqual(box2));

	}
}