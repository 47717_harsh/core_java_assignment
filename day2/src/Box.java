class Box 
{
  //state : DATA : non static data members : mem allocated in heap : after instance creation --instance variables
  //tight encapsulation : data hiding : private
   private double width,depth,height;
   private String color;
   //parameterized ctor to init complete state of the Box
   Box(double w,double d,double height)
   {
	   width=w;
	   depth=d;
	   this.height=height;
   }
   //add another overloaded  ctor to init state of a cube
   Box(double side)
   {
	 this(side,side,side);  
   }
   //add another ctor : to init state =-1
   Box()
   {
		this(-1);	
   }
   Box(double w,double d,double height,String color)
   {
      this(w,d,height);
      this.color = color;
   }
   //Actions : 1. To return Box details in String form (dimensions of Box)
  String getBoxDetails()
   {//adding this keyowrd here : optional , only added for understanding purpose.
	   return "Box dims "+this.width+" "+this.depth+" "+this.height;
   } 
   //2. To return computed volume of the box.
   double getComputedVolume()
   {
	   //this keyword is optional .
	   return width*depth*height;
   }

   boolean isEqual(Box anotherBox)
   {
      return (this.width == anotherBox.width && this.depth == anotherBox.depth && this.height == anotherBox.height);
   }

   Box createNewBox(double wOffset,double dOffset,double hOffset)
   {
      Box newBox = new Box(this.width+wOffset,this.depth+dOffset,this.height+hOffset);
      return newBox;
   }

   String getColor()
   {
      return this.color;
   }
}