package com.app.vehicles;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Vehicle {

	private int chassiNo;
	private String color;
	private double price;
	private Date manufactureDate;
	private String catagory;
	public static SimpleDateFormat sdf;
	static {
		sdf = new SimpleDateFormat("dd/MM/yyyy");
	}

	public Vehicle(int chassiNo, String color, double price, Date manufactureDate) {
		this.chassiNo = chassiNo;
		this.color = color;
		this.price = price;
		this.manufactureDate = manufactureDate;
	}

	public Vehicle(int chassiNo, String color, double price, String catagory, Date manufactureDate) {
		this(chassiNo, color, price, manufactureDate);
		this.catagory = catagory;
	}

	public Vehicle(int chassiNo)
	{
		this.chassiNo = chassiNo;
	}
	
	
	public Date getManufactureDate() {
		return manufactureDate;
	}

	public String getCatagory() {
		return catagory;
	}

	@Override
	public String toString() {
		return "Vehicle [chassiNo=" + chassiNo + ", color=" + color + ", price=" + price + ", manufactureDate="
				+ sdf.format(manufactureDate) + ", catagory=" + catagory + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Vehicle)
			return (chassiNo == ((Vehicle) obj).chassiNo);
		return false;
	}

	public int getChassiNo() {
		return chassiNo;
	}

}
