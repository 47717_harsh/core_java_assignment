package utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.app.vehicles.Vehicle;
import custom_exception.ManufactureDateOutOfRangeException;
import custom_exception.UniqueChassiNoException;
import custom_exception.WrongCatagoryException;

public class ValidateVehicle {

	public static void vehicleValidation(Vehicle[] anVehicle,int chassiNo,String catagory,Date mfgDate) throws UniqueChassiNoException,ManufactureDateOutOfRangeException,WrongCatagoryException, ParseException
	{
		boolean flag = false;
		for(Vehicle v : anVehicle)
		{
			if(v!=null && v.getChassiNo() == chassiNo)
			{
				flag = true;
				break;
			}
		}
		if(flag)
			throw new UniqueChassiNoException("Chassi No already found : Chassi Number should be Unique");
		
		flag = false;
		if(catagory.equalsIgnoreCase("Petrol"))
		{
			flag = false;
		}
		else if(catagory.equalsIgnoreCase("Diesel"))
		{
			flag = false;
		}
		else if(catagory.equalsIgnoreCase("EV"))
		{
			flag = false;
		}
		else if(catagory.equalsIgnoreCase("Hybrid"))
		{
			flag = false;
		}
		else if(catagory.equalsIgnoreCase("CNG"))
		{
			flag = false;
		}
		else
			flag = true;
		
		if(flag)
			throw new WrongCatagoryException("Invalid Catagory selection !!");
		
		Date beforeDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/04/2021");
		Date afterDate = new SimpleDateFormat("dd/MM/yyyy").parse("31/03/2022");
		
		if(!mfgDate.before(beforeDate) && !mfgDate.after(afterDate))
		{
			flag = false;
		}
		else
		{
			flag = true;
		}
		
		if(flag)
			throw new ManufactureDateOutOfRangeException("Manufacture Date must be between(1st Apr 2021 to 31Mar 2022)");
	}
}
