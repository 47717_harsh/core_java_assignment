package utils;

import java.text.ParseException;
import java.util.Date;

import com.app.vehicles.Vehicle;
import custom_exception.ManufactureDateOutOfRangeException;
import custom_exception.UniqueChassiNoException;
import custom_exception.WrongCatagoryException;
import static com.app.vehicles.Vehicle.sdf;

public class VehicleValidationRules {

	public static Date beginDate, endDate;
	static {

		try {
			beginDate = sdf.parse("01/04/2021");
			endDate = sdf.parse("31/03/2022");
		} catch (ParseException e) {
			System.out.println("Error in static init block : " + e);
		}
	}

	public static void validateChassiNo(Vehicle[] anVehicle, int chassiNo) throws UniqueChassiNoException {
		Vehicle newVehicle = new Vehicle(chassiNo);
		for (Vehicle v : anVehicle) {
			if (v != null)
			{
				if(v.equals(newVehicle))
				throw new UniqueChassiNoException("Chassi No already exists : Chassi Number should be Unique");
			}
				
		}
	}

	public static void validateCatagory(String category) throws WrongCatagoryException {
		switch (category) {
		case "Petrol":
		case "Diesel":
		case "EV":
			break;
		default:
			throw new WrongCatagoryException("Invalid Category");
		}
	}

	public static Date parseAndValidateMfgDate(String mfgDate)
			throws ManufactureDateOutOfRangeException, ParseException {
		Date d1 = sdf.parse(mfgDate);
		// parsing success,proceed to validation
		if (d1.before(beginDate) || d1.after(endDate)) {
			throw new ManufactureDateOutOfRangeException("Invalid Date");
		}
		// validation success
		return d1;
	}

	// add static method to invoke all validation rules and throw exception
	public static Vehicle validateAllInputs(Vehicle[] anVehicle,int chassiNo,String color,double price,String catagory,String mfgDate) throws UniqueChassiNoException, WrongCatagoryException, ManufactureDateOutOfRangeException, ParseException
	{
		validateChassiNo(anVehicle,chassiNo);
		validateCatagory(catagory);
		Date d1 = parseAndValidateMfgDate(mfgDate);
		//All inputs are valid
		return new Vehicle(chassiNo, color, price, catagory, d1);
	}

}
