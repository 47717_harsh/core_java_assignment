package tester;

import java.util.Scanner;
import com.app.vehicles.Vehicle;
import static com.app.vehicles.Vehicle.sdf;
public class TestVehicle {

	public static void main(String[] args) throws Exception {
		try (Scanner sc = new Scanner(System.in)) {

			System.out.println("Enter Vehicle detail(chassiNo,color,price,mfgDate(dd/MM/yyyy)) :");
			Vehicle v1 = new Vehicle(sc.nextInt(), sc.next(), sc.nextDouble(),
					sdf.parse(sc.next()));
			Vehicle v2 = new Vehicle(sc.nextInt(), sc.next(), sc.nextDouble(),
					sdf.parse(sc.next()));
			System.out.println(v1.equals(v2));
		}
	}

}
