package tester;

import java.text.ParseException;
import java.util.Scanner;
import com.app.vehicles.Vehicle;

import custom_exception.WrongCatagoryException;

import static utils.VehicleValidationRules.*;

public class Showroom {

	public static void menu() {
		System.out.println("============== SHOWROOM =============");
		System.out.println("1. Add Vehicle");
		System.out.println("2. Display All Vehicles");
		System.out.println("3. Exit");
		System.out.println("Select Choice");
	}

	public static void main(String[] args) throws ParseException {

		try (Scanner sc = new Scanner(System.in)) {

			boolean exit = false;
			int counter = 0;
			System.out.println("Enter Capacity of Showroom : ");
			Vehicle[] vehicleList = new Vehicle[sc.nextInt()];

			while (!exit) {
				menu();
				try {
					switch (sc.nextInt()) {
					case 1:
						if (counter < vehicleList.length) {

							System.out.println(
									"Enter Vehicle Details() (chassiNo,color,price,catagory,mfgDate(dd/MM/yyyy)) :");

							vehicleList[counter++] = validateAllInputs(vehicleList, sc.nextInt(), sc.next(),
									sc.nextDouble(), sc.next(), sc.next());

						} else
							throw new WrongCatagoryException("Showroom full");

						break;
					case 2:
						for (Vehicle v1 : vehicleList) {
							if (v1 != null)
								System.out.println(v1);
						}
						break;
					case 3:
						System.out.println("Bye!!");
						exit = true;
						break;
					default:
						System.out.println("Invalid Choice!!");

					}
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}

		}
	}

}
