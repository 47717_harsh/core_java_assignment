package custom_exception;

@SuppressWarnings("serial")
public class UniqueChassiNoException extends Exception{
	
	public UniqueChassiNoException(String errMsg) {
		super(errMsg);
	}

}
