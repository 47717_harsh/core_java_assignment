package custom_exception;

@SuppressWarnings("serial")
public class ManufactureDateOutOfRangeException extends Exception{
	public ManufactureDateOutOfRangeException(String errMsg) {
		super(errMsg);
	}

}
